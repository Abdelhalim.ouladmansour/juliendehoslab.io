---
title: TP PFW, Déploiement et DevOps
date: 2019-04-16
---


# Devops en Haskell

## Intégration continue, sur Gitlab (devops/mymath)

- Dans le projet fourni, comment sont implémentés les tests unitaires (modules
  de test, configuration, découverte automatique) ?

- Lancez les tests avec Cabal + Nix et vérifiez que tout va bien (ou pas).

- Implémentez et lancez les tests avec Stack. **N'oubliez pas l'option --nix.**

- Dans le projet fourni, comment est implémentée la documentation de code ?

* * *

- Générez et visualisez la documentation avec Cabal + Nix.

- Générez et visualisez la documentation avec Stack.

- Créez un dépôt Gitlab reprenant le projet et écrivez un fichier
  d'intégration continue `.gitlab-ci.yml` qui lance les tests et génère la
  documentation dans un dossier public.

## Déploiement continu simple, sur Heroku

**Reprenez le dépôt Gitlab de l'exercice précédent.**

- Exécutez le programme en local avec Cabal + Nix.

- Exécutez le programme en local avec Stack.

- Déployez le programme sur votre compte Heroku, avec heroku-cli et le build
  pack Stack. N'oubliez pas le fichier `Procfile`.

- Modifiez le fichier `.gitlab-ci.yml` de façon à implémenter du déploiement
  continu.  N'oubliez pas de configurer les variables `HEROKU_API_KEY` et
  `HEROKU_APP_NAME` dans Gitlab.

## Déploiement avec base de données, sur Heroku et sur Nixops (devops/bmxriders)

- Lancez le projet fourni en local. Pour cela, vous aurez besoin d'activer
  Postgresql et de provisionner une base avec le fichier
  `bmxriders_full.sql`.

- Créez un dépôt git reprenant le projet et déployez-le sur Heroku, avec
  heroku-cli. Pour cela, utilisez le build pack Stack et provisionnez une base
  (sur Heroku) avec le fichier `bmxriders.sql`.

- Déployez le projet dans une VM nixops.

# Créer et installer un paquet avec Nixpkgs

## Empaqueter un projet distant (devops/myquitgtk)

Le projet [myquitgtk](https://gitlab.com/juliendehos/myquitgtk)
est un projet C++/cmake classique hébergé sur gitlab.

- Quelle url permet de télécharger une archive tar.gz de la version v0.2 du
  projet ?

- Complétez le fichier `default.nix` de façon à packager cette archive
  (téléchargement, dépendances, compilation...).

- Quelle commande permet de compiler `myquitgtk` en utilisant votre
  fichier Nix ?

- Quelle commande permet d'installer myquitgtk` en utilisant votre
  fichier Nix ?

## Empaqueter un projet local

- Écrivez un fichier default.nix` permettant de compiler le projet
  `devops/myhello` avec Nix. Testez la construction et l'installation.

- Même question pour le projet `devops/myhellogtk`.

# Déployer un site web avec Nixops

## Déploiement d'un site personnel (devops/mypage)

- Le dossier `data` contient une page web simple :

* * *

![](files/devops/mypage.png)

* * *

- Créez un fichier default.nix` permettant d'installer votre page web.

- Complétez le fichier nixops.nix` de façon à déployer votre page web
  dans une virtualbox avec httpd.


## Déploiement d'un serveur web "complet" (devops/pgphptest)

On veut développer un site classique avec serveur web + php + base de données.

- Le projet fourni contient une page PHP qui accède à une base Postgresql, un
  script SQL pour initialiser la base et des fichiers pour packager et déployer
  avec nixops. Vérifiez tout ça et testez le déploiement.

- Modifiez la page PHP et redéployer. Quel est l'inconvénient de cette méthode
  d'un point de vue de développeur web.

* * *

- Pour résoudre ce problème, une méthode consiste à créer, sur la machine de
  déploiement, un dossier spécifique pour le serveur web puis à monter ce
  dossier à distance sur la machine de contrôle. Le développeur web peut alors
  travailler dans le dossier monté sur sa machine de contrôle; les
  modifications seront prises en compte automatiquement sur la machine de
  déploiement :

* * *

#. Connectez vous à la machine de déploiement avec un `nixops ssh
  webserver`.  Créez un dossier `/data/public`, donnez les droits 777 à
  ce dossier et déconnectez-vous. Dans le fichier de déploiement du projet,
  configurez la racine des documents du serveur http à `/data/public` et
  redéployez. Le site devrait fonctionner mais ne trouver aucune page.
#. Sur votre machine de contrôle, créez un dossier de montage `mnt` dans
  votre dossier de projet. Montez le dossier de la machine de déploiement
  avec un `nixops mount webserver:/data/public mnt`. Toujours sur votre
  machine de contrôle, copiez les pages de votre site dans le dossier de
  montage et vérifiez que le site prend directement en compte vos
  modifications.
#. N'oubliez pas de démonter votre dossier de montage avec un `sudo umount
  mnt`.

## Déploiement d'un site wordpress (devops/wptest)

- En utilisant le fichier `wp_nixops.nix`, déployez un wordpress avec apache
  + mysql + un thème. Si le déploiement ne fonctionne pas avec le canal courant, 
  fixez le canal 17.03 dans le fichier de déploiement.

- En utilisant l'interface web, créez un site en français, utilisant le thème
  installé et contenant une page de blog. 

* * *

![](files/devops/wordpress.png)

