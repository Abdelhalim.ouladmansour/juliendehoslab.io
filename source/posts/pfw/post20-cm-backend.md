---
title: CM PFW, Frameworks backend
date: 2019-04-16
---

# Généralités

## Rappels sur les frameworks web backend

- bibliothèques pour développer des applications web
- côté-serveur : le client envoie une requête HTTP, le serveur génère et renvoie une page HTML (ou autre)
- plusieurs types de frameworks :
    - frameworks "lourds"
    - frameworks "légers"

* * *

| langages        | frameworks lourds      | frameworks légers     |
|-----------------|------------------------|-----------------------|
| ruby            | rails                  | sinatra
| python          | django                 | flask
| php             | symfony                | silex
| java            | spring                 | sparkjava
| javascript      | sailsjs, meteor        | expressjs
| haskell         | yesod                  | scotty

## Les frameworks lourds

- objectif : pouvoir implémenter n'importe quelle application web
- fonctionnement :
    - architecture client-serveur
    - REST
    - MVC
- nombreuses fonctionnalités :
    - routage, url mapping
    - génération de pages, templating
    - accès à des bases de données, scaffolding, ORM
    - authentification, cookies, JSON/XML...

## Les frameworks légers

- objectif : pouvoir implémenter rapidement des applications simples et légères
- fonctionnalités de base :
    - architecture client-serveur
    - REST
    - routage
- ajout de fonctionnalités via des bibliothèques, en fonction des besoins

## Les frameworks haskell

- [nombreuses bibliothèques côté-serveur](https://github.com/Gabriel439/post-rfc/blob/master/sotu.md#server-side-programming)  :
    - frameworks lourds : happstack, snap, yesod
    - frameworks légers : scotty, spock, servant
    - serveur http : [warp](https://www.stackage.org/package/warp)
- avantages d'haskell :
    - purement fonctionnel : moins de sources d'erreurs
    - typage statique fort : détection d'erreurs à la compilation

# Scotty

## Généralités

- framework léger utilisant le serveur warp
- extensible avec les bibliothèques haskell classiques (CSS, HTML, base de données...)
- docs :
    - [doc sur scotty](https://hackage.haskell.org/package/scotty)
    - [exemples dans le dépôt de scotty](https://github.com/scotty-web/scotty/tree/master/examples)
    - [read you a scotty](http://devanla.com/read-you-a-scotty.html)


## Exemple de base

```haskell
{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Web.Scotty (scotty, get, html, param, rescue)

main = scotty 3000 $ do
  get "/" $ html "Hello unknown user !"
    -- http://localhost:3000/

  get "/name1/:name" $ do
    name <- param "name"
    html $ L.fromStrict $ T.concat ["Hello ", name, " !"]
    -- http://localhost:3000/name1/toto

  get "/name2" $ do
    name <- param "name" `rescue` (\_ -> return "unknown user")
    html $ L.fromStrict $ T.concat ["Hello ", name, " !"]
    -- http://localhost:3000/name2?name=toto
    -- http://localhost:3000/name2
```

* * *

![](files/backend/scotty_1.png)


## Exemple avec génération de HTML/CSS

```haskell

{-# LANGUAGE OverloadedStrings #-}
import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid
import qualified Web.Scotty as S

divCss = C.div C.# C.byClass "divCss" C.? do
  C.backgroundColor  C.beige
  C.border           C.solid (C.px 1) C.black

main = S.scotty 3000 $ do
  S.get "/" $ S.html $ renderText $ html_ $ do
    head_ $ style_ $ L.toStrict $ C.render divCss
    body_ $ div_ [class_ "divCss"] "bloublou"
```

* * *



![](files/backend/scotty_2.png)


## Exemple de MVC

* * *

- `Model.hs` :

```haskell
{-# LANGUAGE OverloadedStrings #-}

module Model where

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import           Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

dbName = "users.db"

data Name = Name { first :: T.Text, last :: T.Text }

instance FromRow Name where
  fromRow = Name <$> field <*> field

selectUsers :: IO [Name]
selectUsers = do
  conn <- SQL.open dbName
  results <- SQL.query_ conn "SELECT first, last FROM users" :: IO [Name]
  SQL.close conn
  return results
```

* * *

- `View.hs` :

```haskell
{-# LANGUAGE OverloadedStrings #-}

module View (homeRoute) where

import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid

import qualified Model

homeRoute :: [Model.Name] -> L.Text
homeRoute names = renderText $ html_ $ do
  body_ $ do
    h1_ "Names"
    ul_ $ mapM_ (li_ . toHtml . formatName) names
  where formatName name = T.concat [ Model.first name, " ", Model.last name ]
```



* * *

- `Main.hs` :

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Trans (liftIO)
import Web.Scotty (get, scotty, html)

import qualified Model
import qualified View

main = scotty 3000 $ do
  get "/" $ do
    users <- liftIO Model.selectUsers
    html $ View.homeRoute users
```

## Transformateurs de monade

- une monade permet de créer un environnement apportant une "fonctionnalité" particulière

- pour utiliser plusieurs "fonctionnalités" en même temps, on peut imbriquer les monades correspondantes

- les transformateurs de monades permettent d'utiliser une fonction d'une monade dans une monade englobante (lift)

* * *

```haskell 
{-# LANGUAGE OverloadedStrings #-}

import           Control.Monad.Trans (liftIO)
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import qualified Database.SQLite.Simple as S
import           Web.Scotty (get, html, scotty, ActionM, ScottyM)

type Name = (T.Text, T.Text)

dbName = "names.db"
```

* * *

```haskell 
initDb :: IO ()  -- fonction dans la monade IO, qui ne produit aucune valeur
initDb = do
  conn <- S.open dbName
  S.execute_ conn "CREATE TABLE IF NOT EXISTS names (id INTEGER PRIMARY KEY, first TEXT, last TEXT)"
  S.execute conn "INSERT INTO names (first, last) VALUES (?,?)" (("John", "Doe") :: Name)
  S.close conn

selectNames :: IO [Name]  -- fonction dans IO, qui produit une valeur de type [Name]
selectNames = do
  conn <- S.open dbName
  names <- S.query_ conn "SELECT first, last FROM names" :: IO [Name]
  S.close conn
  return names

main :: IO ()
main = do
  initDb  -- appel, dans IO, d'une fonction IO
  scotty 3000 $ get "/" homeRoute

homeRoute :: ActionM ()   -- type ActionM = ActionT Text IO
homeRoute = do 
  names <- liftIO selectNames  -- appel, dans ActionM, d'une fonction IO
  let namesStr = T.intercalate "; " $ map (\ (f,l) -> T.concat [f, " ", l]) names
  html $ L.fromStrict namesStr 
```

## Formulaires

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Trans (liftIO)
import Web.Scotty (get, param, post, redirect, scotty, html)
import Control.Concurrent (newMVar, modifyMVar_, readMVar)
import Lucid

main = do
  myVar <- liftIO $ newMVar ["toto"::String]

  scotty 3000 $ do
    get "/" $ do
      myList <- liftIO $ readMVar myVar
      html $ renderText $ html_ $ body_ $ do
        form_ [action_ "/", method_ "post"] $ do
          input_ [name_ "txt", value_ "enter text here"]
          input_ [type_ "submit"]
        ul_ $ mapM_ (li_ . toHtml) myList

    post "/" $ do
      txt <- param "txt"
      liftIO $ modifyMVar_ myVar (\ v -> return $ v ++ txt)
      redirect "/"
```


## Servir des fichiers statiques

- avec scotty :

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Lucid
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty (get, middleware, html, param, file, scotty)

main = scotty 3000 $ do

  middleware logStdoutDev

  get "/" $ html $ renderText $ html_ $ do 
    body_ $ do
      p_ "exemple pour servir des fichiers avec scotty" 
      img_ [src_ "haskell-logo.png"]

  get "/static/:myfile" $ do
    myfile <- param "myfile"
    file $ "./static/" ++ myfile
```

* * *

- ou mieux, directement avec le middleware :

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Lucid
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import Web.Scotty (get, middleware, html, param, file, scotty)

main = scotty 3000 $ do

  middleware logStdoutDev

  get "/" $ html $ renderText $ html_ $ body_ $ do
    p_ "exemple pour servir des fichiers avec scotty" 
    img_ [src_ "haskell-logo.png"]

  middleware $ staticPolicy $ addBase "static"
```

# Yesod 

## Généralités

- framework "lourd" utilisant le serveur warp (ou autre)
- objectifs :
    - framework modulaire complet
    - syntaxe concise
    - détecter un maximum d'erreurs à la compilation
    - bonnes performances en production
- docs :
    - [site web de yesod](https://www.yesodweb.com)
    - [yesod book](https://www.yesodweb.com/book)

## Exemple de base

pour implémenter d'une application Yesod classique :

- activer les "language pragma" nécessaires
- inclure la bibliothèque Yesod
- créer un type de données de la classe Yesod
- définir les routes et leur contenu
- lancer l'application dans un serveur

![](files/backend/yesod_base.png)

* * *

```haskell
-- active les "language pragma" et inclut la bibliothèque yesod
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
import Yesod

-- crée une application, i.e. un type de données de la classe Yesod
data App = App
instance Yesod App

-- déclare les routes de l'application
mkYesod "App" [parseRoutes|
  /        HomeR     GET
  |]

-- contenu de la route HomeR
getHomeR = defaultLayout $ [hamlet| <p> Welcome to my website |]

-- lance l'application dans un serveur warp sur le port 3000
main = warp 3000 App
```

## Templating

- le code des pages web est généré via des langages dédiés (Domain Specific Language)
- permet de vérifier (à la compilation) que la page est valide
- plusieurs DSL disponibles :
    - whamlet (HTML)
    - cassius (CSS)...
- possibilité de réutiliser les éléments, d'intégrer du code haskell...
- [doc yesod sur les templates](https://www.yesodweb.com/book/shakespearean-templates)

* * *

![](files/backend/yesod_templating.png)

* * *

```haskell
-- déclare du code HTML
linkToMyPage = [whamlet|
  <a href="http://julien.dehos.free.fr"> my page
  |]

-- déclare un style CSS
myStyle = toWidget [cassius|
  .result                         -- style pour la classe "result"
    background: rgb(255,220,150)
  div                             -- style pour la balise "div"
    border: 1px solid black
  |]

getHomeR = defaultLayout $ do
  myStyle                        -- inclut le style CSS
  [whamlet|                      -- déclare du code HTML
    <h1> Home
    <p>
      21 x 2 = #{show $ 21 * 2}  -- exécute du code haskell et inclut le résultat
    <p.result>                   -- utilise une classe définie dans le style CSS
      ^{linkToMyPage}            -- inclut le code HTML déclaré précédemment
    <div>
      ^{linkToMyPage}
    |]
```

## Routage

- permet de déterminer la page à générer, à partir de l'url demandée
- possibilité d'utiliser des paramètres dans l'adresse de routage
- méthode GET ou POST
- vérification des routes à la compilation
- [doc yesod sur le routage](https://www.yesodweb.com/book/routing-and-handlers)

![](files/backend/yesod_routage.png)

* * *

```haskell
-- déclare les routes
mkYesod "App" [parseRoutes|
  /        HomeR     GET     -- route "HomeR" pour l'adresse "/" avec la méthode GET
  /toto    TotoR     GET     -- route "TotoR" pour l'adresse "/toto" avec la méthode GET
  |]

-- gère la route HomeR pour la méthode GET
getHomeR = defaultLayout $ do
  [whamlet|
    <h1> Home
    <a href=@{TotoR}> go to the toto page  -- lien vers la route TotoR
    |]

-- gère la route TotoR pour la méthode GET
getTotoR = defaultLayout $ do
  [whamlet|
    <h1> the toto page
    <a href=@{HomeR}> back home            -- lien vers la route HomeR
  |]
```

## Formulaires

- difficile à gérer correctement :
    - validation et récupération des données saisies
    - failles de sécurité potentielles
    - affichage
- avec Yesod :
    - version "foncteur applicatif" (plus simple)
    - version "monade" (plus évolué, cf doc)
- [doc yesod sur les formulaires](https://www.yesodweb.com/book/forms)

![](files/backend/yesod_forms.png)

* * *

```haskell
import           Control.Applicative ((<$>), (<*>))
import qualified Data.Text as T
-- ...

-- ajoute la gestion des formulaires à l'application
instance RenderMessage App FormMessage where
  renderMessage _ _ = defaultFormMessage

-- type de données pour le formulaire
data Person = Person { name::T.Text, age::Int } deriving Show

-- définit le formulaire (rendu du formulaire et récupération des données)
myForm = renderDivs $ Person
    <$> areq textField "Name" Nothing
    <*> areq intField "Age" (Just 42)

getHomeR = do
  ((result, widget), enctype) <- runFormGet myForm  -- lance la gestion du formulaire
  defaultLayout $ case result of
    FormSuccess person ->  -- des données sont disponibles, affiche les données récupérées
      [whamlet| <p> #{show person} |]
    _ ->                   -- pas de données à récupérer, affiche le formulaire
      [whamlet|
        <form method=get action=@{HomeR} enctype=#{enctype}>
          ^{widget}
          <button> Submit
      |]
```

## Base de données

- modules [Persistent](https://www.yesodweb.com/book/persistent) et 
  [Esqueleto](https://hackage.haskell.org/package/esqueleto)
- compatibles avec plusieurs SGBD : postgresql, mysql, sqlite, mongodb
- modelisation simplifiée, migration de données
- requêtes avec vérification de types 

![](files/backend/yesod_persist.png)

* * *

```haskell
import           Control.Monad.IO.Class (liftIO)
import qualified Data.Text as T
import           Database.Esqueleto 
import           Database.Persist.Sqlite (runSqlite)
import           Yesod

-- nom de fichier pour la base (sqlite)
dbName = "person.db" 

-- définit un type de données persistentes
share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
  Person
    name T.Text
    born Int
    deriving Show
  |]
```

* * *

```haskell
-- fonction qui ajoute quelques données dans la base
initdb :: IO ()
initdb = runSqlite dbName $ do
  runMigration migrateAll
  insert $ Person "John Bonham" 1948
  insert $ Person "Jimi Hendrix" 1942
  johnLennon <- insert $ Person "John Lennon" 1940
  return ()

-- fonction qui récupère les personnes dont le nom commence par un texte donné
selectPersons :: T.Text -> IO [T.Text]
selectPersons str = runSqlite dbName $ do
  runMigration migrateAll
  persons <- select $
             from $ \(p) -> do
             where_ (p ^. PersonName `like` (val $ T.concat [str , "%"]))
             return (p)
  return $ map format persons

-- fonction auxiliaire pour formater les données d'une personne
-- retourne un texte au format : "nom (année)"
format (p) = T.concat [pName, " (", pBorn, ")"]
  where pVal = entityVal p
        pName = personName pVal
        pBorn = T.pack $ show $ personBorn pVal
```

* * *

```haskell
-- application yesod et routage
data App = App
instance Yesod App
mkYesod "App" [parseRoutes|
  /         HomeR     GET
  |]

getHomeR = defaultLayout $ do
  persons <- liftIO $ selectPersons ""    -- sélectionne toutes les personnes

  johns <- liftIO $ selectPersons "John"  -- sélectionne les personnes 

  [whamlet|
    <p> Persons :
    <ul.result> 
      $forall p <- persons    -- parcourt la liste "persons"
        <li> #{p}
    <p> Johns :
    <ul.result> 
      $forall j <- johns      -- parcourt la liste "johns"
        <li> #{j}
    |]

    main = do
      initdb
      warp 3000 App
```

## Plotlyhs

- bibliothèque pour générer des graphiques
- interface (partiellement) la bibliothèque javascript plotly.js
- utilisable avec Yesod, Scotty...
- docs : [plotlyhs](https://hackage.haskell.org/package/plotlyhs), 
  [plotly.js](https://plot.ly/javascript/)
- possibilité d'utiliser une bibliothèque JS "à la main"

![](files/backend/yesod_plotlyhs.png)

* * *

```haskell
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

-- pour les graphiques
import qualified Graphics.Plotly as GP
import           Graphics.Plotly.Lucid (plotlyCDN)
import           Lens.Micro ((&), (.~), (?~))

-- pour la page web
import           Lucid
import           Yesod
import           Yesod.Lucid

data App = App

instance Yesod App

mkYesod "App" [parseRoutes|
  /    HomeR    GET
  |]

main = warp 3000 App
```

* * *

```haskell
-- données à afficher 
pointsData :: [(Double, Double)]
pointsData = zip [1, 2, 3, 4] [50, 300, 70, 20]

-- fonction pour générer le graphique (avec plotlyhs)
myPlot = GP.plotly "myDiv" 
  [ GP.line (GP.aes & GP.x .~ fst & GP.y .~ snd) pointsData & GP.name ?~ "toto" ]
  & GP.layout . GP.height ?~ 300
  & GP.layout . GP.margin ?~ GP.thinMargins
  & GP.layout . GP.showlegend ?~ True

-- page pour afficher le graphique 
getHomeR = lucid $ \url -> do
  html_ $ do
    header_ $ plotlyCDN
    body_ $ do
      h1_ $ "test plotlyhs"
      Lucid.toHtml myPlot 
```

