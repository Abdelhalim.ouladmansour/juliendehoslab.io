---
title: TP PFW, Frameworks backend
date: 2019-04-16
---

## Scotty : Découverte (backend/scotty/helloscotty)

À partir du projet fourni, implémentez un serveur web fournissant deux pages :

- une page d'accueil
- une page `hello` permettant de saisir un nom et d'afficher un message personnalisé.

* * *

<video preload="metadata" controls>
<source src="files/backend/pfw_helloscotty.mp4" type="video/mp4" />
![](files/backend/pfw_helloscotty.png)

</video>

## Scotty : HTML/CSS + PostgreSQL (backend/scotty/bmxriders)

À partir du projet fourni, implémentez un serveur web qui affiche une gallerie
d'images.  Indication : implémentez d'abord un type `Bmxrider` et une fonction
`getBmxriders` pour récupérer les données dans la base, puis implémentez la
génération du code HTML.

* * *

<video preload="metadata" controls>
<source src="files/backend/pfw_scotty_bmxriders.mp4" type="video/mp4" />
![](files/backend/pfw_scotty_bmxriders.png)

</video>

## Scotty : MVC + HTML/CSS + SQLite (backend/scotty/poemhub)

Complétez le projet fourni de façon à pouvoir :

- lister une base de données de poèmes
- afficher les différents poèmes
- ajouter un nouveau poème

* * *

<video preload="metadata" controls>
<source src="files/backend/pfw_poemhub.mp4" type="video/mp4" />
![](files/backend/pfw_poemhub.png)

</video>

## Yesod : Découverte (yesod/helloyesod)

On veut implémenter une application web contenant une page de chiffrement de
César et une page de recherche dans une base de données de musique.

- Complétez le code fourni (`helloyesod.hs`) : pages manquantes, routes, page
  d'accueil, styles CSS...

* * *

<video preload="metadata" controls>
<source src="files/backend/helloyesod.mp4" type="video/mp4" />
![](files/backend/helloyesod.png)

</video>

## Yesod : Génération de graphiques (yesod/someplots)

Le projet fournit implémente une application web qui affiche quelques
graphiques, en utilisant lucid, plotlyhs et yesod.

- La page "Meteorites" affiche les positions GPS d'attérissage de météorites.
  Modifiez le code du fichier `Main.hs` de façon à conserver uniquement les
  points de latitude 15 à 50 et de longitude -130 à -70.

- Complétez la page "SpuriousCorrelation" (cf vidéo ci-dessous).

Sources : 
[spurious correlation](http://www.tylervigen.com/spurious-correlations), 
[NASA](https://data.nasa.gov/Space-Science/Meteorite-Landings/gh4g-9sfh)

* * *

<video preload="metadata" controls>
<source src="files/backend/someplots.mp4" type="video/mp4" />
![](files/backend/someplots.png)

</video>

