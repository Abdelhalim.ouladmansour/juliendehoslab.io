---
title: CM PFW, CGI
date: 2019-04-16
---

# Généralités

## CGI

- Common Gateway Interface
- permet à un serveur web d'exécuter un programme (Perl, C...) et de retourner le résultat au client
- vieux (1993) et de moins en moins utilisé
- mais simple à mettre en oeuvre (bibliothèques Perl/C..., modules CGI dans apache/lighttpd...)
- évolution avec FastCGI (plus performant)

![](files/cgi/dessin_cgi.png){width="90%"}

## Au niveau du client web

- le client demande une url classique via HTTP
- 2 méthodes possibles pour les paramètres d'entrée :
    - GET : paramètres passés dans l'url, par exemple `http://.../script.cgi?param1=val1&param2=val2`
    - POST : paramètres passés dans le corps de la requête HTTP
- le client reçoit la réponse (texte, page HTML...) via HTTP

## Au niveau du serveur

- le programme cible est appelé par l'interface CGI du serveur web :
    - avec la méthode GET, les paramètres sont passés dans la variable d'environnement `QUERY_STRING`
    - avec la méthode POST, les paramètres sont passés dans l'entrée standard du programme cible
- le programme écrit sa réponse (après un entête) dans la sortie standard :

```html
Content-type:text/html

<!DOCTYPE html>
<html>
  <body>
    blou
  </body>
</html>
```

# Scripts CGI en Python

## Fonctionnalités de base

- entrées/sorties classiques (`print`, `input`)
- variables d'environnement avec la bibliothèque `os`

```python
import os
query_string = os.environ["QUERY_STRING"]

print("Content-type:text/html")
print("")
print("<!DOCTYPE html>")
print("<html>")
...
```

## Bibliothèque Python CGI

- permet de récupérer les paramètres d'entrée
- [doc Python CGI](https://docs.python.org/3.6/library/cgi.html)

```html
<form action="myscript.py" method="POST">
<input type="input" name="email" />
...
</form>
```

```python
import cgi
form = cgi.FieldStorage()
email = form.getvalue('email')
...
```

## Déploiement (Nixops + lighttpd)

- ajouter le module `mod_cgi`
- spécifier l'interpréteur python

```nix
services.lighttpd = {
  ...
  enableModules = [ "mod_cgi" ];
  extraConfig = ''
    cgi.assign = ( ".py"  => "${pkgs.python3}/bin/python" )
    '';
};
```

# Scripts CGI en Haskell

## Fonctionnalités de base

- sortie standard avec `putStrLn`

```haskell
main = do
  putStrLn "Content-type: text/html"
  putStrLn ""
  putStrLn "<!DOCTYPE html>"
  putStrLn "<html>"
  ...
```

## Bibliothèque Haskell CGI

- gère les paramètres d'entrée, l'entête de sortie...
- [doc Haskell CGI](https://hackage.haskell.org/package/cgi-3001.3.0.2/docs/Network-CGI.html)

```haskell
import Network.CGI

main :: IO ()
main = runCGI $ handleErrors cgiMain

cgiMain :: CGI CGIResult
cgiMain = do
  email <- getInput "email"
  output $ renderPage email

renderPage :: Maybe String -> String
renderPage email = "<!DOCTYPE html>\n"
  ++ "<html>\n"
  ...
```

## Déploiement avec lighttpd

- compiler un binaire avec l'extension `.cgi`
- indiquer à lighttpd une exécution directe : `cgi.assign = ( ".cgi"  => "" )`

