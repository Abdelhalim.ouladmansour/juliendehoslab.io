---
title: TP PFW, CGI
date: 2019-04-16
---

## Scripts CGI simples (cgi/simple_scripts)

Implémentez en Python les scripts CGI suivants et déployez-les avec nixops en utilisant un serveur lighttpd :

- `hello` : construit et renvoie une page de type "helloworld"
- `print_captain` : page affichant le contenu du fichier `captain.txt` lu sur le serveur 
- `print_env` : page contenant les variables d'environnement du serveur
- `hello_name` : page utilisant un paramètre du script CGI
- `cgi_field` : page utilisant les paramètres du script CGI, via la bibliothèque [Python CGI](https://docs.python.org/3.5/library/cgi.html#higher-level-interface)

* * *

<video preload="metadata" controls>
<source src="files/cgi/simple_scripts.mp4" type="video/mp4">
![](files/cgi/simple_scripts_6.png)

</video>

## Génération d'images (cgi/make_img)

Complétez le script CGI `make_img.py` de façon à générer un page web
contenant une image de damier, de taille de carreaux paramétrée. Déployez avec
nixops.

Indications:

- pour le script CGI, utilisez la bibliothèque Python CGI
- pour calculer l'image de damier, utilisez le fichier `utils.py`
- créez l'image de damier dans le répertoire `/tmp` en utilisant un lien symbolique local 

* * *

<video preload="metadata" controls>
<source src="files/cgi/make_img.mp4" type="video/mp4">
![](files/cgi/make_img_1.png)

</video>

## Script CGI en Haskell (cgi/hellohs)

Le dossier principal contient une page html (dossier `data`), un projet haskell
ghc (`helloghc`) et un projet haskell cabal (`hellocabal`). Ces deux
projets contiennent un packaging Nix et le dossier principal également (qui
inclut les deux précédents).

- Complétez le fichier `helloghc.hs` de façon à faire un calcul sur le
  serveur (par exemple : $21 \times 2$) et retourner le résultat dans une
  page html via CGI.
- Complétez le fichier `hellocabal.hs` de façon à faire la même chose mais en
  utilisant la bibliothèque XHtml.

* * *

![](files/cgi/hello_1.png)

## Chiffrement de César (cgi/caesar)

Le projet Haskell fourni utilise cabal et nix/nixops. L'exécutable
`hello.cgi` calcule une page d'accueil via CGI.

- Implémentez un nouvel exécutable `hellohtml.cgi` identique à `hello.cgi`
  mais utilisant la bibliothèque `Text.XHtml`.
- Terminer l'implémentation de `caesar.cgi` de façon à calculer le
  chiffrement de César d'un texte donné avec une clé donnée. 

* * *

<video preload="metadata" controls>
<source src="files/cgi/caesar.mp4" type="video/mp4">
![](files/cgi/caesar_2.png)

</video>

## Lecture d'une base de données (cgi/music)

Le projet fourni déploie une base de données de titres de musique et un
programme CGI en Haskell qui y accède.

- Testez le projet fourni.
- Modifiez le programme `list.cgi` de façon à ajouter, pour chaque titre, le
  nom de l'artiste.
- Ajouter un second programme `search.cgi` permettant de faire une recherche
  parmi les titres.

* * *

![](files/cgi/music_cgi.png)

