---
title: TP HPC, OpenMP
date: 2019-04-15
---

## Introduction

Le but de ce TP est d'utiliser OpenMP pour calculer des images et d'aborder
quelques problèmes classiques de programmation parallèle (organisation et
répartition des calculs, mémoire partagée/privée)

> **Important :**
> 
> - Si ce n'est pas déjà fait, forkez/clonez <br> le 
>   [dépôt de code du module HPC](https://gitlab.com/juliendehos/M_HPC_etudiant). 
> - Travaillez dans le dossier `hpcOpenmp`.
> - Ouvrez un `nix-shell` et compilez avec `make`

## Découverte d'OpenMP

- Expliquez brièvement ce qu'il faut faire pour paralléliser une boucle avec
   OpenMP (algorithme, directive de code, option de compilation).

- Le fichier `hpcOpenmpFibo.cpp` implémente le calcul séquentiel de la suite
   $F^{42}$ du TP précédent. Modifiez le code pour paralléliser la boucle
   de calcul. Pensez à équilibrer la charge.

```text
[nix-shell]$ ./hpcOpenmpFibo.out 20

[nix-shell]$ cat output.txt 
0 1 1 2 3 5 8 13 21 34 13 5 18 23 41 22 21 1 22 23 
```

## Répartition des calculs

Une image en niveaux de gris peut être implémentée par un tableau contenant le
niveau de chaque pixel.  Le fichier `hpcOpenmpSchedule.cpp` génére un dégradé en
diagonal dans le tableau puis écrit l'image dans un fichier PNM. 

* * *

- Testez le programme et affichez l'image obtenue.

```text
[nix-shell]$ ./hpcOpenmpSchedule.out 400 300 toto.pnm
400 300 0.00690525

[nix-shell]$ eog toto.pnm 
```

![Image initiale.](files/tp-openmp/TP_OpenMP_ex2a.png){width="400px"}

* * *

- Parallélisez la boucle sur 3 threads et changez le calcul de façon à
   affecter, à chaque pixel, une couleur identifiant le thread qui le calcule.

![Répartition des calculs par blocs verticaux.](files/tp-openmp/TP_OpenMP_ex2b.png){width="400px"}

* * *

- Spécifiez une répartition par segments de 50 pixels.

![Répartition des calculs par blocs de 50 pixels.](files/tp-openmp/TP_OpenMP_ex2c.png){width="400px"}

* * *

- Faites désormais la parallélisation sur la boucle imbriquée. Qu'observez-vous
   au niveau du temps de calcul ? Pourquoi ? Conclusion ?

![Répartition des calculs par blocs horizontaux.](files/tp-openmp/TP_OpenMP_ex2d.png){width="400px"}

## Accès concurrent, ressources privées/partagées

Le fichier `hpcOpenmpRandom.cpp` implémente un programme qui génère une image aléatoire.

![Image aléatoire calculée séquentiellement.](files/tp-openmp/TP_OpenMP_ex4a.png){width="400px"}

* * *

- En vous inspirant fortement de l'implémentation séquentielle, implémentez le
   calcul en parallèle.  Comparez les temps de calcul. Que constatez-vous ?
   Comment l'expliquez-vous ?

* * *

- Réimplémentez votre parallélisation en utilisant le mécanisme de variables
   privées d'OpenMP.  Analysez le résultat obtenu (temps de calcul, image
   générée).

![Parallélisation naïve.](files/tp-openmp/TP_OpenMP_ex4c.png){width="400px"}

* * *

- Réimplémentez votre parallélisation de façon à corriger le problème
   précédent.

![Parallélisation correcte.](files/tp-openmp/TP_OpenMP_ex4d.png){width="400px"}

* * *

```text
[nix-shell]$ ./hpcOpenmpRandom.out 4000 4000
width = 4000
height = 4000
nb proc = 24
delta time (sequential)                              = 0.716739
delta time (parallel, same RNG)                      = 0.743276
delta time (parallel, different RNG, same seed)      = 0.0952502
delta time (parallel, different RNG, different seed) = 0.0814635
```

## Filtrage d'images

Le fichier `hpcOpenmpFilter.cpp` implémente un programme qui génère une image
de damier puis en calcule une image floutée grâce à une convolution par le
noyau : 

$$\frac{1}{256} 
\begin{bmatrix} 
1 & 4 & 6 & 4 & 1 \\  
4 & 16 & 24 & 16 & 4 \\ 
6 & 24 & 36 & 24 & 6 \\ 
4 & 16 & 24 & 16 & 4 \\ 
1 & 4 & 6 & 4 & 1
\end{bmatrix}$$

Pour calculer une convolution, on obtient chaque pixel de l'image de sortie en
calculant la moyenne des pixels voisins, dans l'image d'entrée, pondérée par
les coefficients du noyau centré sur ce pixel.

* * *

![Damier initial.](files/tp-openmp/TP_OpenMP_ex3a.png){width="400px"}

* * *

![Damier après convolution.](files/tp-openmp/TP_OpenMP_ex3b.png){width="400px"}

* * *

- En vous inspirant fortement de l'implémentation séquentielle, implémentez la
   convolution en parallèle.  Justifiez la validité de votre implémentation
   parallèle.

- En remarquant que le noyau de convolution est égal au produit 

    $$\frac{1}{16}
    \begin{bmatrix} 
    1 & 4 & 6 & 4 & 1 
    \end{bmatrix} 
    . 
    \frac{1}{16}
    \begin{bmatrix} 
    1 & 4 & 6 & 4 & 1 
    \end{bmatrix}^T$$

   (noyau séparable), proposez un autre algorithme pour calculer la convolution et
   implémentez-le en utilisant OpenMP.  

* * *

- Comparez les temps de calcul obtenus avec les trois implémentations (cf
   exemple suivant).  Qu'en concluez-vous ?

```text
$ ./hpcOpenmpFilter.out 4000 4000 
width = 4000
height = 4000
nb proc = 2
delta time (sequential)        = 0.577716
delta time (parallel)          = 0.375486
delta time (parallel+separate) = 0.196539
speed-up (parallel)            = 1.53858
speed-up (parallel+separate)   = 2.93945
```


