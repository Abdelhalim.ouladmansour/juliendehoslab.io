---
title: CM HPC, Généralités
date: 2019-04-15
---

# Motivation

## Définition du HPC

- High Performance Computing (calcul à haute performance)
- typiquement : gros calculs réalisés sur des supercalculateurs
- mais également : systèmes distribués ou à accès concurrents, applications
  nécessitant beaucoup de calculs sur des ordinateurs personnels ou des
  appareils mobiles...

* * *

![Tianhe-2 (2013).](files/cm-hpc/tianhe2.png)

* * *

![Réalité augmentée sur smartphone.](files/cm-hpc/smartphone-ar.jpg)


## Notion de puissance de calcul

- quelques unités de mesure :
    - FLOPS : FLoating-point Operations Per Second
    - GFLOPS : $10^9$ FLOPS
    - PFLOPS : $10^{15}$ FLOPS

* * *

- quelques exemples :
    - CPU intel Core i7-3770 (2013) : 200 GFLOPS
    - GPU AMD Radeon R9 290X (2013) : 5\ 632 GFLOPS
    - Sony PS4 (2013) : 102 GFLOPS CPU + 1\ 843 GFLOPS GPU
    - Apple iPhone 6 (2014) : 166 GFLOPS GPU
    - Tianhe-2 (2013) : 33,86 PFLOPS $\approx$ 200K iPhone 6 

* * *

- se méfier des chiffres annoncés :
    - FLOPS en simple ou double précision
    - puissance moyenne ou en crête
    - prise en compte des initialisations, transferts de données...
    - comparaison d'implémentations avec le même degré d'optimisation

## Évolution de la puissance de calcul

- la fréquence d'horloge plafonne à 3 GHz depuis 2005
- le nombre de transistors continue à doubler tous les 2 ans (loi de Moore, 1975)
- nouvelles contraintes : limites de miniaturisation, dissipation thermique, consommation électrique

$\rightarrow$ nécessité du parallélisme

* * *

![Intel CPU Trends.](files/cm-hpc/cpu-trends.jpg)

## Besoin en puissance de calcul : prévisions Météo-France

- supercalculateur NEC SX9 (2009) : 40 TFLOPS en crête
- trois modèles de simulation couplés, différentes échelles
- réajustement toutes les 6 heures

![Nec SX9 (2009).](files/cm-hpc/nec_sx9.jpg)

## Besoin en puissance de calcul : jeu d'échecs

- nombre de feuilles estimé de l'arbre de jeu : $10^{123}$
- en considérant qu'un ordinateur à 100 PFLOPS peut calculer une feuille en
  1 opération, il faudrait plus de $10^{96}$ siècles pour tout
  calculer 

![Kasparov vs Deep Blue (1997).](files/cm-hpc/kasparov.jpg)

## Besoin en puissance de calcul : jeux vidéo

- calculer des images $1920 \times 1080$ en 17 ms $\approx$ 124M pixels/s
- à partir de scènes 3D de plusieurs millions de polygones
- et en gérant l'éclairage, les physics...

![Crysis 3 (2013).](files/cm-hpc/crysis3a.png)

## Exemple d'applications

![](files/cm-hpc/llnl_applications1.jpg)

![](files/cm-hpc/llnl_applications2.jpg)

## Exemple de calculateur : IBM Sequoia (2012) 

- 96 racks, 1024 noeuds/rack, 16 coeurs/noeud = 1 572 864 coeurs (PowerPC A2
  1.6 GHz)
- mémoire RAM : 16 GB/noeud = 16 PB 
- interconnexion en anneau 5D
- puissance de calcul : 16.3 PFLOPS
- OS basé linux
- surface occupée : 300 m$^2$
- poids : 200 t 
- consommation électrique : 7.9 MW soit 2 GFLOPS/W
- coût : \$650M soit environ \$43 le GFLOPS

* * *

![IBM Sequoia, vue générale.](files/cm-hpc/ibm_sequoia_1.jpg)

* * *

![IBM Sequoia, contenu des armoires.](files/cm-hpc/ibm_sequoia_2.jpg)

* * *

![IBM Sequoia, contenu des racks.](files/cm-hpc/ibm_sequoia_3.jpg)

* * *

![IBM Sequoia, construction.](files/cm-hpc/ibm_sequoia_4.jpg)

* * *

![IBM Sequoia, système de refroidissement.](files/cm-hpc/ibm_sequoia_5.jpg)

## Exemple de calculateur : Sony PS3 (2006)

- processeur CELL (6 coeurs dispo $\approx$ 150 GFLOPS SP vect. pip.)
- (+ 1 processeur graphique à 400 GLOPS)
- même époque : 1 CPU Intel Core 2 Duo $\approx$ 20 GFLOPS
- OS basé BSD
- consommation électrique : 200 W soit 0.75 GFLOPS/W
- coût : \$500 soit environ \$3 le GFLOPS (\$43 pour Sequoia) $\rightarrow$ moralité : achetez japonais

![Sony PlayStation 3 (2006).](files/cm-hpc/ps3.jpg)

* * *

- et en plus ça passe bien à l'échelle :

![Réseau de PlayStation 3.](files/cm-hpc/ps3_cluster.jpg)

# Architectures parallèles/distribuées

## Systèmes distribués

- "noeuds" hétérogènes de calcul ou de stockage 
- traitement de tâches hétérogènes
- exemple : services web (HTTP+BD+traitements)
- contraintes de fiabilité, capacité, charge, temps de réponse...

* * *

![Architecture distribuée.](files/cm-hpc/systeme_distribue_1.jpg)

## Calculateurs massivement parallèles 

- supercalculateurs, grilles de calcul, ordinateurs en réseau, 
- nombre de processeurs important
- calculs sur des gros volumes de données
- exemple : prévisions météo, synthèse d'images, cryptanalyse...

* * *

![Calculateur parallèle.](files/cm-hpc/landfire-hargrove.jpg)

* * *

![Ferme de calcul.](files/cm-hpc/Army-1005.jpg)

## Systèmes multi-processeurs

- plusieurs processeurs dans une même machine
- CPU, GPU...
- intérêts : mémoire partagée, interconnexion haute performance
- mais nombre de processeurs faible

![Nvidia Tesla CXT8000).](files/cm-hpc/CXT8000-st.png)

## Processeurs multi-coeurs

- plusieurs coeurs parallèles dans un même processeur
- spécifique par coeur : unités de contrôle/calcul + mémoire 
- partagé : unités de contrôle communes + mémoire partagée 

![Intel i7.](files/cm-hpc/multi_core_CPU.jpg)

## Processeurs vectoriels

- calculer une instruction sur plusieurs données en même temps
- avec des registres 128 bits, on peut traiter 4 données 32 bits à la fois
- sur les processeurs x86 : SSE, AVX

$\rightarrow$ permet d'accélérer les calculs mais nécessite une programmation particulière 

* * *

![Architecture SIMD.](files/cm-hpc/simd2.png)

## Architecture pipeline

- réaliser un calcul nécessite plusieurs étapes
- sans pipeline : on effectue toutes les étapes d'un calcul puis on passe au
  calcul suivant
- avec pipeline : lorsqu'un calcul passe à l'étape suivante, on remplit l'étape
  libérée avec un nouveau calcul, sans attendre

$\rightarrow$ permet d'accélérer les calculs mais nécessite une programmation particulière (garder le pipeline plein)

* * *

![Architecture sans pipeline.](files/cm-hpc/pipeline_1.svg){width="90%"}

* * *

![Architecture avec pipeline.](files/cm-hpc/pipeline_2.svg){width="55%"}

## Ces architectures sont complémentaires

- calculateur à $N_1$ noeuds
- noeuds à $N_2$ processeurs
- processeurs à $N_3$ coeurs
- coeurs à $N_4$ unités de calcul (UC)

$= N_1 \times N_2 \times N_3 \times N_4$ unités de calcul

+ de la mémoire spécifique à chaque niveau

+ des techniques de programmation spécifiques à chaque niveau

$\rightarrow$ une grande puissance de calcul et une bonne migraine pour
programmer tout ça

# Notions et problématiques de base 

## Parallélisme de tâches ou de données

- parallélisme de tâches :
    - le système effectue en parallèle des tâches/calculs différents
    - exemples : services web, ordinateur personnel multi-tâche
    - programmation concurrente
- parallélisme de données :
    - le système effectue le même calcul, sur des données différentes
    - exemples : supercalculateurs, GPU
    - programmation parallèle

## Dans ce module : parallélisme de données

- réaliser un calcul sur plusieurs "processeurs" en parallèle
- intérêt : accélérer le calcul, pouvoir traiter plus de données 
- en général, la difficulté n'est pas d'implémenter le calcul mais de le faire
  efficacement
- [Lawrence Livermore National Laboratory](https://computing.llnl.gov/tutorials/parallel_comp)

## Accélération (speed-up)

- accélération : $S(P) = \frac{T(1)}{T(P)}$
- cas idéal : $S(P) = P$ c'est-à-dire $T(P) = \frac{T(1)}{P}$

* * *

![Exécution séquentielle](tikz/calcul_sequentiel.svg){width="70%"}

![Exécution parallèle (idéale)](tikz/calcul_parallel.svg){width="40%"}

## Équilibrage de charge

- l'accélération est maximale quand la charge est répartie de façon équilibrée :
    - $S(P) = P \quad \Rightarrow$ charge équilibrée
    - $S(P) < P \quad \Rightarrow$ charge peut-être non-équilibrée

* * *

![Charge équilibrée](tikz/schema_equilibrage1.svg){width="30%"}

![Charge non équilibrée](tikz/schema_equilibrage2.svg){width="50%"}


## Passage à l'échelle (scalability)

- évolution de l'accélération en fonction du nombre d'unités de calcul 
- avec peu d'UC, la parallélisation est souvent efficace
- à partir d'un certain seuil, utiliser plus d'UC n'accélère pas le calcul,
  voire le ralentit (coût de gestion de la parallélisation, limite de
  parallélisation du calcul...)

* * *

![Passage à l'échelle](tikz/calcul_scalability.svg){width="40%"}

## Loi d'Amdahl

- décrit le passage à l'échelle théorique selon le calcul à réaliser

- le calcul peut être décomposé en :
    - une partie parallélisable sur $P$ unités de calcul (de proportion
      $\tau$)
    - une partie séquentielle résiduelle (de proportion $1-\tau$)

* * *

![Composition d'un calcul](tikz/calcul_amdahl.svg){width="80%"}

- on a : $T(P) = \frac{\tau T(1)}{P} + (1-\tau) T(1)$

- d'où (loi d'Amdahl) : $S(P) = \frac{T(1)}{T(P)} = \frac{1}{\frac{\tau}{P} +
  1-\tau}$

$\rightarrow$ un calcul passe bien à l'échelle uniquement si $\tau$ est très proche de 1

* * *

![Loi d'Amdahl pour quelques portions parallèles $\tau$.](files/cm-hpc/AmdahlsLaw.svg){width="80%"}

# Mise en oeuvre

## Avant de paralléliser/optimiser du code

- est-ce nécessaire (performances insuffisantes) ?
- l'algorithme déjà implémenté est-il optimal ?
- gestion mémoire correcte (structures de données, allocation...) ?
- options de compilation, assertions, logs... ?
- profilage de code (où est consommé le temps) ?
- efficacité des optimisations réalisées (mesurer) ?

## Processus, thread

- processus :
    - programme en cours d'exécution
    - espace mémoire réservé au processus
    - communication entre processus par messages/signaux
- thread :
    - duplication "légère" d'un processus
    - espace mémoire partagé par tous les threads du processus
    - communication entre threads d'un processus par appels systèmes

* * *

![Communication inter-processus et inter-threads](tikz/schema_processus.svg){width="80%"}

## Architecture à mémoire distribuée

- chaque unité de calcul accède à sa propre mémoire
- exemples : systèmes distribués, grilles de calcul, supercalculateurs
- plus évolutif mais moins efficace et plus difficile à utiliser
- programmation : socket, MPI

* * *

![Architecture à mémoire distribuée.](files/cm-hpc/distributed_mem.png) 

## Architecture à mémoire partagée

- toutes les unités de calcul accèdent à la même mémoire
- exemples : multi-CPU, CPU multi-coeurs, GPU
- également appelé SMP (Symmetric Multi-Processing)
- efficace et simple à utiliser mais peu évolutif
- accès mémoire pas forcément uniforme
- programmation : thread, OpenMP

* * *

![Architecture à mémoire partagée.](files/cm-hpc/shared_mem.png) 

## Différents niveaux/technologies de programmation parallèle

- entre noeuds : thread/socket, MPI
- dans un noeud : thread, OpenMP
- dans un coeur : SSE, AVX

* * *

![Système avec mémoire distribuée et mémoire partagée.](files/cm-hpc/hybrid_model.png) 

## Modèle d'exécution "fork and join"

- on crée des threads pour répartir les calculs
- on exécute les threads en parallèle, sur des coeurs différents
- on attend la fin de l'exécution de tous les threads pour continuer

* * *

![Exécution de tâches selon un modèle fork & join.](files/cm-hpc/Fork_join.png) 

## Difficultés liées à la parallélisation

- résultats toujours corrects ?
- algorithme adapté à la parallélisation ?
- charge de calcul équilibrée ?
- accès concurrent à une ressource partagée (lecture/écriture mémoire, appel
  système) ?
- surcoût de la parallélisation (création/communication des threads, transfert en mémoire distribuée) ?

## Répartition des calculs

![Découpage du domaine en tâches.](files/cm-hpc/llnl_domain_decomp.png)

* * *

![Quelques types de découpage en tâches.](files/cm-hpc/llnl_distributions.png)

## Accès concurrent (race condition)

- plusieurs processus/threads accèdent à une ressource (périphérique, mémoire
  partagée...) en même temps
- problèmes possibles :
    - calculs incorrects (utilisation de données obsolètes)
    - faibles performances (attentes)
    - interblocages (attente mutuelle)
- exemple d'accès concurrent :

```cpp
#omp parallel for num_threads(2)
for (int i=0; i<N; i++)
{
    int x = rand();  // appel système bloquant et concurrent
    T[i] = f(x);
}
```

* * *

![Accès concurrents à `rand`](tikz/schema_concurrence.svg){width="90%"}

