---
title: TP HPC, MPI
date: 2019-04-15
---

## Introduction

Le but de ce TP est d'utiliser MPI pour faire du calcul parallèle (calcul
d'intégrales, traitement d'images) sur architecture à mémoire distribuée.  Les
programmes que vous allez développer devraient pouvoir être exécutés sur des
noeuds de calcul en réseau mais pour simplifier, vous exécuterez sur votre
machine multi-coeur locale.

Documentations :

- [page MPI du LLNL](https://computing.llnl.gov/tutorials/mpi/)
- [tutorial mpi4py](https://mpi4py.readthedocs.io/en/stable)

## Découverte de MPI 

> **Important :**
> 
> Pour cet exercice, allez dans le dossier `hpcMpiCpp`, 
> ouvrez un `nix-shell` et compilez avec `make`.

* * *

- Comment compile-t-on un programme MPI ?

- Comment exécute-t-on  un programme MPI ? 

- Compilez et exécutez les fichiers `hpcMpiCppHello1.cpp` et `hpcMpiCppHello2.cpp`. 

```text
[nix-shell]$ mpirun -n 4 ./hpcMpiCppHello1.out 
Hello, I am process #0
Hello, I am process #1
Hello, I am process #3
Hello, I am process #2
...
```

* * *

- Regardez l'implémentation du modèle fork-and-join dans le fichier `hpcMpiCppHello2.cpp`. 

- Regardez et testez les implémentations équivalentes en Python (dossier `hpcMpi`).

```text
$ nix-shell --run "mpirun -n 4 hpcMpiHello1.py"
Hello, I am process #0
Hello, I am process #3
Hello, I am process #1
Hello, I am process #2
```


## Calcul parallèle simple 

> **Important :**
> 
> Pour cet exercice et les suivants, allez dans le dossier `hpcMpi` et <br>
> testez vos programmes avec des `nix-shell --run`.

* * *

Le fichier `hpcMpiIntegralSeq.py` calcule l'intégrale $\int_0^1
\frac{4}{1+x^2}dx \approx \pi$ par la méthode des rectangles.  Comme le calcul
n'était pas assez inefficace, on a rajouté un peu de $F^{42}$.
Maintenant que ce calcul est coûteux, on propose de le paralléliser grâce à la
relation :

$$\int_a^b f(x) dx = \int_a^i f(x) dx + \int_i^b f(x) dx$$

* * *

- Testez l'implémentation séquentielle fournie :

```text
$ nix-shell --run "hpcMpiIntegralSeq.py 0.0001"
0.0001 1 3.14157849407382 0.3852391242980957
```

- Ré-implémentez le calcul en parallèle (dans un fichier `hpcMpiIntegralPar.py`).
  Votre implémentation devra utiliser la fonction de réduction de MPI et afficher
  **UNE** ligne indiquant le nombre de processus de calcul, le résultat final
  et le temps d'exécution total.

```text
$ nix-shell --run "mpirun -n 4 hpcMpiIntegralPar.py 0.0001"
0.0001 4 3.1415884871165436 0.16899800300598145
```

- En utilisant les scripts fournis, calculez et analysez le passage à l'échelle
  de votre programme.

```text
$ nix-shell --run hpcMpiIntegralRun.sh
$ nix-shell --run "hpcMpiPlot.py outIntegral.csv"
```

* * *

![Passage à l'échelle sur un processeur 4 coeurs.](files/tp-mpi/scalabilityIntegral.png)



## Calcul parallèle avec transfert de données 1D

Le fichier `hpcMpiImageSeq.py` calcule la convolution d'une image par un
filtre moyenneur de rayon paramétrable.  

- Testez le programme séquentiel.

```text
$ nix-shell --run "hpcMpiImageSeq.py 20"
20 1 4.12873649597168

$ eog backloop.pnm output.pnm
```

* * *

- Écrivez une implémentation parallèle MPI (dans un fichier
  `hpcMpiImagePar.py`).  Utilisez la fonction `Gather` de MPI et considérez
  que le fichier image est accessible localement. Votre programme affichera le
  nombre de processus de calcul, le rayon du filtre et le temps de calcul
  total.  Il prendra comme argument en ligne de commande le rayon du filtre et
  répartira le calcul par blocs de colonnes de l'image. 

```text
$ nix-shell --run "mpirun -n 4 hpcMpiImagePar.py 20"
20 4 1.0679895329994906

$ eog backloop.pnm output.pnm
```

* * *

![Image d'entrée.](files/tp-mpi/backloop.png)

* * *

![Image de sortie calculée avec 8 processus (filtre de rayon 20).](files/tp-mpi/backloop_blur.png)

* * *


- Calculez et analysez le passage à l'échelle de votre programme.

```text
$ nix-shell --run hpcMpiImageRun.sh
$ nix-shell --run "hpcMpiPlot.py outImage.csv"
```

* * *

![Passage à l'échelle sur un processeur 4 coeurs (filtre de rayon 20).](files/tp-mpi/scalabilityImage.png)

## S'il vous reste du temps

- Implémentez le filtrage en C++/OpenMP et comparez les temps de calculs.

- Implémentez le filtrage en découpant l'image par blocs 2D.

* * *

![Parallélisation par blocs 2D (filtre de rayon 2).](files/tp-mpi/output.png)


