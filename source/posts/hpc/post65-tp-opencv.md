---
title: TP HPC, OpenCV
date: 2019-04-16
---

## Introduction

Dans ce TP, vous allez utiliser la bibliothèque de traitement d'images OpenCV 3
en Python.  De par la taille des données et la complexité des algorithmes, le
traitement d'images peut vite devenir coûteux. Le but du TP est de voir comment
utiliser ce genre de bibliothèque efficacement (temps de calcul, temps de
développement).

Pensez à consulter la [documentation d'OpenCV 3](http://docs.opencv.org),
notamment la section *OpenCV-Python Tutorials*.

## Comparaison C++/Python avec du filtrage d'images

Le filtrage est une opération très classique en traitement d'images. Elle
permet, par exemple, de flouter une image.

* * *

![image initiale](files/tp-opencv/kermorvan_small.png)

![image filtrée](files/tp-opencv/kermorvan_small_umat.png)

* * *


- Allez dans le dossier `hpcOpencvCpp` et testez le programme C++
  `hpcOpencvCppBlur` :

```text
$ nix-shell --cores 8

[nix-shell]$ make

[nix-shell]$ ./src/hpcOpencvCppBlur.out data/parrots.jpg out.png 201 100
time: 3.93335 s

[nix-shell]$ exit
```

* * *


- Allez dans le dossier `hpcOpencv`, copiez le fichier
  `scripts/hpcOpencvTest.py` en un fichier `scripts/hpcOpencvBlur` et
  modifiez cette copie de façon à implémenter un programme équivalent au
  programme `hpcOpencvCppBlur` précédent.

```text
$ nix-shell --cores 8

[nix-shell]$ ./scripts/hpcOpencvBlur.py data/parrots.jpg out.png 201 100
time: 3.9453718662261963 s
```

- Comparez les temps de calcul entre la version C++ et la version Python.

## Multiplication et affichage d'une image

- Implémentez un programme qui lit et affiche une image multipliée par un
  coefficient. Le nom de l'image et le coefficient doivent être donnés en
  paramètre et le programme doit quitter quand on appuie sur la touche <esc>.

![Affichage de `kermorvan.png` avec une multiplication par 0.5.](files/tp-opencv/mul.png)


## Détection de contours dans un flux vidéo

- Implémentez un programme qui lit un fichier vidéo et qui détecte et affiche
  les contours.  Indication : flou gaussien de taille 9 et de sigma 1.5,
  détecteur de Canny de seuils 0 et 40.

![Détection de contour dans la vidéo `bmx.mkv`.](files/tp-opencv/bmx.gif)

* * *


- Implémentez un programme identique mais pour un flux vidéo venant d'une caméra.

## Seuillage interactif

- Implémentez un programme qui lit une image passée en paramètre et affiche son
  image seuillée d'une valeur réglable par un trackbar.

![Seuillage interactif de `kermorvan.png`.](files/tp-opencv/thres.png)


## Calcul d'histogrammes

- Implémentez un programme qui calcul les histogrammes (en log) d'une image
  donnée en paramètre et l'affiche avec matplotlib. 

* * *

![Calcul et affichage de l'histogramme d'une image.](files/tp-opencv/hist.png)

## Suivi de motif dans un flux vidéo

- Implémentez un programme de template matching dans un flux vidéo.
  Inspirez-vous de l'exemple fourni dans les tutoriaux d'OpenCV.

* * *


![Recherche du template `road_auto.png` dans la vidéo `road_traffic.mp4` (méthode `TM_CCOEFF_NORMED` avec un seuil de 0,7).](files/tp-opencv/tracking.gif){width="80%"}


## Reconnaissance de chiffres manuscrits

- Le dossier `hpcMnist` contient un programme Tensorflow + Keras pour
  entrainer un réseau de neurones sur la base MNIST. Lancez l'apprentissage du modèle.

```text
$ nix-shell -A cpu --cores 8

[nix-shell]$ python mnist_train.py 
Train on 60000 samples, validate on 10000 samples
Epoch 1/3
12352/60000 [=====>........................] - ETA: 15s - loss: 0.2602 - acc: 0.9229
```

- Testez la prédiction en utilisant le script `mnist_test.py` et l'image `test.jpg`.

* * *

- Écrivez un script `mnist_gui.py` qui permet de dessiner dans une
  fenêtre et d'en prédire le chiffre correspondant. Indications :
  `cv.setMouseCallback`, `cv.line`...

<video preload="metadata" controls>
<source src="files/tp-opencv/hpc_mnist.mp4" type="video/mp4">
![Programme de reconnaissance de chiffres manuscripts.](files/tp-opencv/hpc_mnist.png)

</video>

## Serveur de webcam

- Le dossier `hpcWebcam` contient un programme serveur en Haskell + Scotty +
  OpenCV. Branchez une webcam, lancez le programme et allez à `localhost:3000`
  dans un navigateur web. **Pensez à indiquer plusieurs cœurs pour votre nix-shell**.

* * *

<video preload="metadata" controls>
<source src="files/tp-opencv/hpc_webcam_1.mp4" type="video/mp4">
![Serveur de webcam sans traitement du flux.](files/tp-opencv/hpc_webcam_1.png)

</video>

* * *

- Ouvrez le port 3000 de votre machine et testez l'accès à votre programme
  serveur depuis une autre machine du réseau local. Pour cela, ajoutez l'option 
  suivante dans votre `/etc/nixos/configuration.nix` (n'oubliez pas de lancer
  un `sudo nixos-rebuild switch` ensuite).

```nix
networking = {
    firewall.allowedTCPPorts = [ 3000 ];
    ...
};
```

* * *

- Regardez le code fourni et intégrez la fonction suivante pour traiter le flux vidéo.

```haskell
process :: ImageD -> ImageD
process img = exceptError $ (coerceMat :: (ImageD -> CvExcept ImageS)) img
    >>= gaussianBlur (V2 9 9) 1.5 1.5 
    >>= canny 0 40 (Just 3) CannyNormL1 
    >>= cvtColor gray bgr 
    >>= coerceMat 
```

* * *

<video preload="metadata" controls>
<source src="files/tp-opencv/hpc_webcam_2.mp4" type="video/mp4">
![Serveur de webcam avec traitement du flux.](files/tp-opencv/hpc_webcam_2.png)

</video>

* * *

- Augmentez la taille du filtre (par exemple, 49). Vérifiez avec un `htop` que les calculs sont répartis sur plusieurs cœurs.


