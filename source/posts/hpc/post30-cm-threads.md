---
title: CM HPC, Threads
date: 2019-04-15
---

## Principe des threads

- "processus léger"
- section de code d'un processus qui s'exécute de façon indépendante 
- mémoire partagée par tous les threads d'un processus + mémoire privée 
- exécution gérée par le système (si possible en parallèle)
- communication entre threads par appel système (create, exit, join...)
- différentes implémentations : pthread, rthread...
- programmation parallèle et programmation concurrente

* * *

![Mémoire partagée, mémoire privée.](files/cm-threads/sharedMemoryModel.png) 

## Les threads C++11

- API du langage C++11 (option de compilation `std=c++11`)
- code portable (utilise l'implémentation de thread du système cible)
- entête `<thread>`
- créer un thread : classe `std::thread`
- attendre la fin d'un thread : méthode `join`
- nombre de coeurs : fonction globale `std::thread::hardware_concurrency`
- (id du thread courant : méthode `std::this_thread::get_id`)
- http://en.cppreference.com

## Modèle "fork and join" en C++11

```cpp
#include <thread>
// fonction de calcul 
void calculer(int i0, int i1) {
    for (int i=i0; i<i1; i++)
        ...
}
// fonction principale
int main() {
    ...
    // lance deux threads de calcul en parallèle
    std::thread thread1(calculer, 0, N/2);  // appel non-bloquant
    std::thread thread2(calculer, N/2, N);  // appel non-bloquant
    ...
    // attend la fin des calculs
    thread1.join();  // appel bloquant
    thread2.join();  // appel bloquant
    ...
}
```

## Partage mémoire en C++11

```cpp
void calculer(std::vector<int> & refData) {
    for (int i=0; i<refData.size(); i++)
        refData[i] = ...    // écriture en mémoire partagée
}
int main() {
    std::vector<int> data;
    ...
    std::thread thread1(calculer, std::ref(data));
    // attention : possibilité d'accès concurrent (data)
    ...
    thread1.join();
    // exécution de thread1 terminée
    ...
}
```


