---
title: Présentation du module Génie Logiciel
date: 2019-04-18
---

## Objectifs

Initiation au génie logiciel :

- connaître quelques généralités : cycle de vie, UML…
- analyser un besoin, concevoir une solution, etc…
- mettre en pratique des méthodes et outils classiques pour développer un projet

## Pré-requis

- programmation orientée objet
- environnement unix

## Organisation

- 6h de CM/TD
- 30h de TP
- 6h de projet (projet en commun avec le module C++)

## Évaluation

- controle continu

## Travaux pratiques

- Vous aurez besoin d'un [compte gitlab](https://gitlab.com/users/sign_in).
- Forkez le [dépôt git fourni](https://gitlab.com/juliendehos/L3_GL_etudiant). 
- **L'utilisation de linux est imposée**. Vous pouvez utiliser geany, emacs ou vim pour éditer du texte et qtcreator pour les projets C++/CMake.

* * *

> Si vous voulez absolument utiliser votre machine perso, installez les paquets
> suivants et **vérifiez que votre installation fonctionne avant les
> séances de TP** :
> `geany` `qtcreator` `curl` `wget` `gnuplot` `epstool` `imagemagick` `bash`
> `git` `g++` `make` `cmake` `doxygen` `python-sphinx` `libopencv-dev`
> `libgoogle-glog-dev` `libcpputest-dev` `graphviz` `libvlc-dev` `vlc` `eog`
> `file` `tree` `libboost-all-dev` `python3-virtualenv` `pybind11-dev`
> `libgirepository1.0-dev` `python3-tk`

