---
title: TP GL, Tests et erreurs 
date: 2019-04-18
---

Un développeur doit faire face à deux types d'erreur : 

- les erreurs de programmation (bugs) : ces erreurs ne devraient pas se
  produire et doivent être corrigées (on peut les détecter par des assertions,
  tests unitaires...).

- les erreurs d'utilisation : ces erreurs peuvent se produire et doivent être
  gérées (par des exceptions, codes d'erreur...).


## Assertions

Les assertions permettent de vérifier les préconditions, postconditions et
invariants d'une fonction "depuis l'intérieur". 

- Ouvrez un terminal dans le dossier `TP_tests`.

- Compilez et exécutez le programme `print_fibo.out` du projet `fibo`.
  Expliquez le problème.

- En utilisant la fonction `assert`, implémentez deux assertions permettant
  de vérifier que les termes de la suite sont positifs et croissants.  Vérifiez
  que les assertions détecte le problème précédent.

- Compilez avec l'option `-DNDEBUG` et vérifiez que les assertions sont
  désactivées.


## Tests unitaires

Les tests unitaires permettent de vérifier la validité d'une fonction "depuis
l'extérieur".

- Ajoutez des tests unitaires au projet `fibo` en utilisant la bibliothèque
  `cpputest` (cf le [module de C++](../cpp/post10-cm.html#tests-unitaires) ou la 
  [documentation de cpputest](https://cpputest.github.io/)).  Vérifiez par
  exemple que les cinq premières valeurs de la fonction `fibo` sont correctes.

- Remplacez les `assert` de l'exercice précédent par des exceptions
  (`throw`) et écrivez des tests unitaires permettant de vérifier que des
  exceptions sont bien levées en cas de paramètre invalide.


## Codes d'erreur vs exceptions

Pour indiquer qu'une erreur utilisateur a été détectée, une fonction peut
retourner un code d'erreur. Ce mécanisme est beaucoup utilisé dans les
bibliothèques C et permet de gérer explicitement les erreurs.  Les codes
d'erreur sont cependant assez fastidieux à utiliser donc on préfère souvent
utiliser des exceptions, hormis dans quelques situations particulières (systèmes temps-réels...).

* * *

- Compilez le projet `readfile`. Exécutez le programme correspondant au
  fichier `readfile_exception.cpp` et le script `run.sh`. Comment sont
  gérées les différentes erreurs ?

- Complètez le programme `readfile_errno.cpp` en utilisant les codes
  d'erreur.  Faites attention à la gestion dynamique de la mémoire. Testez le
  script `run.sh` sur votre programme.

- Modifiez le programme `readfile_exception.cpp` de façon à utiliser un
  `vector` au lieu de l'allocation dynamique. 


## Exceptions

Les exceptions permettent de gérer des erreurs utilisateur. Elles permettent
notamment de rendre le code plus lisible (en séparant le cas nominal des cas
d'erreur) et plus robuste (pas d'oubli dans les tests de code d'erreur)...

- Compilez le projet `drawspline` et testez-le avec le script `plot.sh`.

- Le fichier `Spline.cpp` implémente une gestion d'erreur grossière, avec des
  assertions. Réimplémentez cette gestion d'erreur avec des exceptions. Pour
  simplifier, vous pouvez lever des exceptions de type `std::string` et tout
  capturer dans le programme principal.

- Exécuter le programme sur des cas d'erreur (nombre de clés insuffisant, clés
  mal ordonnées...)

* * *

![](files/TP_tests_drawspline.svg)


## Logs

Un fichier de log permet d'enregistrer une trace d'exécution d'un programme.
Les logs peuvent contenir différents types de message (INFO, WARNING, ERROR)
voire même interrompre le programme (FATAL). Ils sont souvent utilisés dans des
logiciels propices aux erreurs utilisateur (système d'exploitation, serveur
web...).

- En utilisant [glog](http://rpg.ifi.uzh.ch/docs/glog.html) (disponible via `pkg-config`), ajoutez des logs d'information dans le programme principal de `drawspline` (cf les commentaires de code). Indication : après l'initialisation de glog, ajoutez la ligne suivante pour spécifiez le nom des fichiers de log.

```cpp
google::SetLogDestination(google::GLOG_INFO, "log_drawspline");
```


* * *

- Ajoutez des logs d'erreur dans le module `Spline`, en même temps que les
  exceptions. Vérifiez les logs produits dans un cas d'erreur.

```text
Log file created at: 2016/12/20 15:23:20
Running on machine: narges
Log line format: [IWEF]mmdd hh:mm:ss.uuuuuu threadid file:line] msg
I1220 15:23:20.927736 42044 drawspline.cpp:11] begin
I1220 15:23:20.927939 42044 drawspline.cpp:14] create spline
I1220 15:23:20.927947 42044 drawspline.cpp:17] add keys
E1220 15:23:20.928011 42044 Spline.cpp:11] keys should be added in ascending times
I1220 15:23:20.928205 42044 drawspline.cpp:35] end
```


## S'il vous reste du temps

- Écrivez des tests unitaires pour les modules `Vec2` et `Spline` du projet
  `drawspline` (pour éviter des problèmes de compatibilité, n'initialisez pas
  glog pour le programme de tests unitaires). Pensez à tester que les
  exceptions sont bien levées.

- Écrivez une documentation de code pour `drawspline`.


