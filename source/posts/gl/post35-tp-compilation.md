---
title: TP GL, Compilation et debug 
date: 2019-04-18
---

> **Important :**
> 
> - Si ce n'est pas déjà fait, forkez le 
>   [dépôt fourni](https://gitlab.com/juliendehos/L3_GL_etudiant) 
>   sur <br> votre compte gitlab et clonez votre dépôt.
> - Committer/pusher votre travail régulièrement <br> (à la fin de
>   chaque exercice, par exemple).

# Outils de compilation

## Compilation séparée

- Ouvrez un terminal et allez dans le dossier `TP_compilation/sayHello`.

- Compilez le projet `sayHello` en une seule ligne de commande. Pensez aux
  options classiques (fichier de sortie, warnings, standard du langage,
  optimisation...)

- Écrivez un script exécutable `compile.sh` qui effectue cette compilation.

* * *

- Quelles sont les différentes étapes de la «compilation» ? Recompilez le
  projet `sayHello` en réalisant chaque étape explicitement. Quel est
  l'intérêt de réaliser les différentes étapes séparément ?

- Modifiez votre script `compile.sh` pour réaliser les étapes de compilation
  séparément. Écrivez un script `clean.sh` qui supprime les fichiers
  produits.

## Compilation avec des bibliothèques externes

- Quelle est la bibliothèque utilisée par le projet `imshow` ? Quels fichiers
  sont nécessaires et où sont-ils situés ?

- Compilez le projet dans un terminal.

- Le programme `pkg-config` permet de récupérer des informations sur les
  bibliothèques (voir le 
  [guide pkg-config](https://people.freedesktop.org/~dbn/pkg-config-guide.html#using)).
  Testez les options `--list-all`, `--cflags` et `--libs`.

- Recompilez le projet `imshow` en utilisant `pkg-config`.
  Indications : utilisez la notation "backtick" du bash.

## Rappels sur les Makefile

- Comment utilise-t-on un Makefile pour compiler un projet ? Comment fonctionne
  le système de règle des Makefile ?  Si besoin, consultez les deux premières
  sections de la 
  [doc gnu make](https://www.gnu.org/software/make/manual/make.html#Introduction).

- Écrivez un Makefile basique pour compiler le projet `sayHello` (une règle
  par fichier objet/binaire à produire). Vérifiez que le projet compile bien et
  que seuls les fichiers nécessaires sont recompilés lors des modifications.

- Réécrivez votre Makefile en utilisant des règles automatiques et «flags»
  classiques (la gestion des dépendances envers les fichiers d'entête n'est pas
  demandée).

- Écrivez un Makefile pour le projet `imshow`.

## Compilation avec CMake

Le programme `make` permet de compiler un projet à partir d'un fichier de
configuration de projet (Makefile). Les Makefile ne sont pas toujours
portables : il faut parfois les modifier pour pouvoir compiler sur un autre
système.  Le programme `cmake` permet de générer un Makefile adapté au système
courant, à partir d'un autre type de fichier de configuration de projet
(CMakeLists.txt).

La compilation avec `cmake` consiste donc à écrire un CMakeLists.txt, exécuter
`cmake` pour générer le Makefile puis exécuter `make` pour compiler le projet.
Voir la doc pour [écrire un CMakeLists.txt](https://cmake.org/cmake-tutorial/),
pour [compiler avec cmake](https://cmake.org/runningcmake/), et pour 
[inclure des bibliothèques](https://cmake.org/Wiki/CMake:How_To_Find_Libraries).

* * *

- Écrivez un CMakeLists.txt pour le projet `sayHello` et compilez le projet
  en utilisant `cmake` et `make`.

- Idem pour le projet `imshow`.

- Que faut il faire pour recompiler le projet si on modifie un fichier ? Et si
  on ajoute un fichier ? 

- Testez les programmes `ccmake` et `cmake-gui`. Regardez notamment les
  options de compilation utilisées et les chemins de bibliothèque trouvés.

## Compilation via des bibliothèques statiques 

Le projet `play_video` contient un projet plus réaliste, composé d'un module
`play_video` et de deux programmes principaux `play_video_gui` et
`play_video_test` utilisant ce module. Pour optimiser la compilation, une
méthode classique est de compiler le module dans une bibliothèque statique et
d'inclure celle-ci ensuite lors de la compilation des programmes.

- Écrivez un CMakeLists.txt correspondant. Indication : incluez les
  bibliothèques externes `gtkmm` et `libvlc` via `pkg-config` et incluez
  `boost` directement via sa configuration `cmake`.

# IDE (QtCreator)

## Utilisation avec CMake

- Dans QtCreator, créez un projet CMake, en partant de zéro :

    - allez dans l'onglet « Accueil »
    - choisissez « Nouveau projet », « Non-Qt Project », « Plain C++ Application »
    - mettez le nom de projet "helloworld" et le chemin que vous voulez
    - sélectionnez « CMake » comme « build system » 
    - laissez les autres paramètres par défaut puis faites « Terminer »
    - regardez les fichiers créés et les « messages généraux »
    - exécutez votre programme (icone « flêche verte », en bas à gauche)
    - fermez votre projet avec le menu « Fichier », « Close project "helloworld" »

* * *

- Dans QtCreator, ouvrez un projet CMake simple, déjà existant :

    - allez dans l'onglet « Accueil »
    - choisissez « Ouvrir le projet »
    - sélectionnez le fichier `CMakeLists.txt` du projet `sayHello`
    - terminez avec « Configure Project »
    - exécutez le projet et vérifiez le résultat affiché
    - fermez le projet

* * *

- Dans QtCreator, ouvrez un projet CMake plus complexe :

    - ouvrez le projet CMake `play_video`
    - sélectionnez la configuration `play_video_gui.out` (icone « écran », en bas à gauche), exécutez et regardez l'erreur obtenue
    - allez dans l'onglet « Projets », « Run » et réglez le « working directory » et le « command line arguments »
    - vérifiez que l'exécution fonctionne désormais

## Interface de base

- Ouvrez le project CMake `play_video`.

- Repérez les différents éléments de l'interface :

    - onglets des vues (en haut à gauche) : « Accueil », « Editer », « Projets »…
    - boutons de configuration, compilation, exécution (en bas à droite)
    - fenêtres de messages (en bas à droite)
    - fenêtre d'édition (en haut à droite) 

* * *

- Testez la navigation de code avec ctrl + clic gauche, dans la fenêtre d'édition

- Testez les erreurs de compilation : ajoutez une erreur dans le code, compilez et regardez le message d'erreur (fenêtre « problèmes » et fenêtre « sortie de compilation »).

## Debug (1)

- Ouvrez le project `play_video`.

- Sélectionnez la configuration « Debug »  et le programme `play_video_gui.out`.

- Ajoutez un point d'arrêt dans la fonction `getFilesInFolder` du fichier
  `Filesytem.cpp`.

- Lancez le debug et vérifiez que QtCreator passe sur la vue « Debug ».

* * *

- Repérez les différentes fenêtres de la vue « Debug » : variables locales, pile d'appels, liste des points d'arrêts, 

- Faites une exécution pas-à-pas jusqu'à revenir dans le `main`, tout en
  suivant l'évolution des variables locales (notamment le tableau
  `filenames`).

- Stoppez le debug et revenez dans la vue « Editer ».

## Debug (2)

- Ouvrez le project `sayHello`.

- Ouvrez la fenêtre de recherche avancée (ctrl + maj + f) et cherchez la fonction
  `getHello` dans le projet.

- Décommentez les deux lignes, dans la fonction `getHello`.

- Lancez le debug (n'oubliez pas de sélectionner la configuration correspondante).

- Vérifiez que le programme détecte un segfault et que le debugger vous indique
  la ligne correspondante.

- Placez un point d'arrêt sur cette ligne et relancez le debug.

* * *

- Avant l'exécution de la ligne, modifiez la valeur du pointeur `ptr` de
  façon à le faire pointer vers la variable `n` (utilisez le « memory
  editor » pour trouver son adresse).

- Continuez l'exécution et vérifiez que le programme termine correctement.

- Pensez à recommenter les deux lignes.

## S'il vous reste du temps

- Testez les `autotools` sur le projet 
  [GNU Hello](https://www.gnu.org/software/hello/) (configuration, compilation,
  installation dans un dossier utilisateur). Voir la 
  [doc sur automake](https://www.gnu.org/software/automake/manual/automake.html#Basic-Installation).

