---
title: TP GL, Workflow (6h)
date: 2019-04-18
---

> **Attention :**
>
> Vous disposez de 2 séances pour réaliser ce TP. <br>
> Prenez donc le temps de bien comprendre le code <br>
> fourni et d'utiliser correctement les méthodes <br>
> et outils demandés.

* * *

L'objectif de ce TP est de réaliser un projet de développement logiciel
en utilisant des outils et méthodes classiques : cycle itératif,
git, doxygen, tests unitaires, intégration/déploiement continu…

Le projet à réaliser est un jeu de tictactoe avec une interface texte et une
interface graphique. Voir le 
[travail de spécification réalisé en TD](post25-td-uml.html#étude-de-cas).

<video preload="metadata" controls>
<source src="files/gl-workflow-tictactoe.mp4" type="video/mp4" />
![](files/gl-workflow-tictactoe.png)

</video>

## Mise en place du projet

Dépôt git/gitlab :

- Créez un dépôt `tictactoe` sur votre compte gitlab. Copiez et ajoutez les
  fichiers fournis (`L3_GL_etudiant/TP_workflow/`) dans votre nouveau dépôt.
  N'oubliez pas le fichier `.gitignore` mais n'ajoutez pas le fichier
  `.gitlab-ci.yml` pour l'instant. Commitez/pushez et vérifiez ces
  modifications sur l'interface web de gitlab.

* * *

Intégration continue :

- Compilez le projet CMake manuellement (sans QtCreator) et testez les
  programmes (interface texte, tests unitaires).

- Copiez et ajoutez le fichier d'intégration continue `.gitlab-ci.yml` à
  votre dépôt. Regardez et comprenez ce qu'il contient. Commitez/pushez et
  regardez le résultat sur l'interface web de gitlab (onglet « CI/CD »,
  rubriques « Pipelines » et « Jobs »).

- Regardez et comprenez le job « pages » : description dans le
  `.gitlab-ci.yml`, « Job artifacts » dans l'interface web et déploiement sur
  l'URL `gitlab.io`.

* * *

- Modifiez le code C++ de façon à y ajouter une erreur de compilation.
  Commitez/pushez et regardez ce qu'il se passe au niveau de l'intégration
  continue. 

- Corrigez l'erreur de compilation, ajoutez une erreur dans les tests unitaires
  et regardez le résultat de l'intégration continue.

* * *

Documentation de code :

- Générez une documentation de code avec Doxygen. Le fichier de configuration 
  est fourni mais vous devrez reformater les commentaires de code.

- Modifiez la configuration de l'intégration continue de façon à générer et
  déployer la documentation de code. Pour cela, ajoutez un job de type build et
  modifiez le job « pages ». Vérifiez le résultat sur l'URL de déploiement
  `gitlab.io`.

* * *

Interface graphique (Python ou JavaScript) :

- Pour développer l'interface graphique du tictactoe, on propose de réutiliser
  le code C++ en le compilant via Boost Python ou WebAssembly. Regardez et
  comprenez le code fourni, notamment l'interfaçage du code C++ et les scripts.

- Avec Python : regardez les documentations [intégration de cairo](https://pygobject.readthedocs.io/en/latest/guide/cairo_integration.html)
  et [tutorial Python Gtk](https://python-gtk-3-tutorial.readthedocs.io/en/latest/).

- Avec JavaScript : modifiez la configuration de l'intégration continue
  de façon à générer le code WebAssembly et à déployer l'interface web du
  tictactoe. Pour cela, vous pouvez utilisez la même image Docker que pour le
  TP sur les scripts.  Vérifiez le résultat sur l'URL de déploiement. 
  
## Réalisation du projet

Pour réaliser le projet, on propose la planification suivante :

- Milestone 1 : moteur de jeu
    - Issue 1 : affichage de la grille (validation : Tests Unitaires)
    - Issue 2 : jouer des coups, avec détection de case libre mais sans détection de victoire/égalité (validation : TU)
    - Issue 3 : détection de victoire (validation : TU)
    - Issue 4 : détection d'égalité (validation : TU)

* * *

- Milestone 2 : interface texte
    - Issue 5 : afficher un jeu (validation : exécution manuelle)
    - Issue 6 : saisir un coup (validation : exécution manuelle)
    - Issue 7 : dérouler une partie de jeu complète (validation : exécution manuelle)

* * *

- Milestone 3 : interface graphique (JavaScript ou Python)
    - Issue 8 : intégration du moteur de jeu (validation : manuelle)
    - Issue 9 : implémentation de l'interface graphique (validation : manuelle)
    - Issue 10 : affichage d'un message de status (validation : manuelle)

* * *

Dans l'interface web de gitlab, ajoutez les milestones et issues prévus, puis
réalisez le projet selon la démarche suivante : 

- choisissez l'issue à traiter
- écrivez les code/tests/doc correspondant à l'issue (pas plus, pas moins) 
- effectuez la validation (tests unitaires, tests manuels…)
- commitez, pushez, vérifiez l'intégration continue et fermez l'issue
- si l'issue termine un milestone, ajoutez un tag
- recommencez avec l'issue suivante


