---
title: CM C++, Standard Template Library
date: 2019-04-19
---

## Généralités sur la STL

- bibliothèque de templates standard du C++ :
    - chaînes de caractères
    - entrées/sorties
    - conteneurs, itérateurs, objets-fonctions, algorithmes
    - threads, expressions régulières, nombres pseudo-aléatoires...
- généralement, à utiliser sans modération
- <http://en.cppreference.com>

## Chaines de caractères

- classe `string`
- compatibles avec les chaînes C
- compatibles avec les conteneurs STL
- et nombreuses fonctionnalités

```cpp
#include <string>
#include <iostream>
int main() {
    std::string s1("toto");
    std::string s2 = "tata";
    std::string s3 = s1 + s2;
    std::string s4 = s3.substr(0, s3.find('a'));
    std::cout << s4 << std::endl;
    return 0;
}
```

## Entrées/sorties

- flux de données avec : écran/clavier, fichiers, chaînes de caractères...
- opérateurs de flux (surdéfinissables) :
    - `>>` (opérateur de flux d’entrée)
    - `<<` (opérateur de flux de sortie)
- classes d’e/s de la bibliothèque standard :
    - `ifstream` (lecture depuis un fichier)
    - `ofstream` (écriture vers un fichier)
    - `stringstream` (flux avec une chaîne de caractère)…
- objets d’e/s de la bibliothèque standard :
    - `cin` (entrée standard)
    - `cout` (sortie standard)
    - `cerr` (sortie d’erreur)…

## Entrées/sorties : istream, lecture fichier

- classe de base pour les flux d’entrée
- opérateur `>>` (utilisable en cascade)
- la fonction `std::getline` est également bien pratique
- objet de la bibliothèque standard : `cin`

* * *

```cpp
// lecture dans un fichier
#include <fstream>
#include <iostream>

int main() {

    std::ifstream is("test.txt");
    // crée un flux d'entrée vers un fichier

    int nombre;
    while (is >> nombre) { // lit dans le flux
        std::cout << nombre << std::endl;
    }

    return 0;
} // le fichier est fermé automatiquement à 
  // la destruction de is
```

* * *

```cpp
// exemple de signature pour surdéfinir l'opérateur >>
std::istream & operator>>(std::istream & is,
                          MaClasse & x);
```

## Entrées/sorties : ostream, écriture fichier

- classe de base pour les flux de sortie
- opérateur `<<` (utilisable en cascade)
- objet de la bibliothèque standard : `cout` `cerr` `clog`
- les écritures sont bufferisées (vidage par sauts de ligne et par `flush`)

* * *

```cpp
// écriture dans un fichier
#include <fstream>
#include <iostream>

int main() {

    // crée un flux de sortie vers un fichier
    std::ofstream os("test.txt");

    if (os) {
        os << 13 << std::endl; // écrit dans le flux
        os << 37 << std::endl;
    }

    return 0;
} // le fichier est fermé automatiquement à 
  // la destruction de os
```

* * *

```cpp
// exemple de signature pour surdéfinir l'opérateur <<
std::ostream & operator<<(std::ostream & os,
                          const MaClasse & x);
```

## Entrées/sorties : stringstream

- flux depuis/vers une chaîne de caractère
- utile pour extraire/formater des données

* * *

```cpp
#include <iostream>
#include <sstream>
int main() {

    // produit des données dans un flux de chaine
    std::stringstream ss;
    ss << 42 << "toto";

    // lit la chaine correspondant au flux
    std::string s1 = ss.str();
    std::cout << s1 << std::endl;

    // consomme des données du flux
    int n;
    ss >> n;
    std::cout << n << std::endl;

    return 0;
}
```

## Conteneurs

- classes/templates permettant de stocker des données
- la STL propose différents conteneurs : `array vector list set map` ...
- chaque conteneur dispose de fonctionnalités particulières via ses méthodes
- et de fonctionnalités communes via les itérateurs et algorithmes

## Conteneurs : array

- version propre des tableaux statiques
- compatible avec les passages de paramètres par valeur/référence et avec les itérateurs

* * *

```cpp
#include <array>
#include <iostream>
void afficherArray(const std::array<int,3> & a) {
    for (int x : a)              // paramètre par référence
        std::cout << x << ' ';
    std::cout << std::endl;
}
std::array<int,3> creerArray() { // retour par valeur
    std::array<int,3> a {0, 0, 0};
    return a;
}
int main() {
    auto a = creerArray();
    afficherArray(a);
    a[1] = 42;
    afficherArray(a);
    return 0;
}
```

## Conteneurs : vector

- tableau dynamique
- accés aléatoire rapide
- insertion/suppression rapide en fin (modulo la réallocation mémoire)
- compatible avec les tableaux C
- en cas de doute sur le choix d’un conteneur, prendre un vector

* * *

```cpp
#include <iostream>
#include <vector>

int main() {

    std::vector<int> V {13, 37};
    // crée et initialise un tableau à 2 éléments

    V.push_back(42); // ajoute un élément au tableau

    for (unsigned i=0; i<V.size(); i++)
        std::cout << V[i] << std::endl;

    return 0;
}
```

## Conteneurs : list

- liste chaînée
- insertion en temps constant
- pas d’accès aléatoire

* * *

```cpp
#include <iostream>
#include <list>

int main() {

    std::list<int> L {13, 37};

    L.push_back(42);
    L.pop_front();

    for (int x : L)
        std::cout << x << std::endl;

    return 0;
}
```

## Conteneurs : set

- conteneur d’éléments uniques
- recherche en temps logarithmique

* * *

```cpp
#include <iostream>
#include <set>

int main() {

    std::set<int> S {13, 37};

    S.insert(42);

    auto iter = S.find(37);
    if (iter != S.end())
        std::cout << "trouvé" << std::endl;
    else
        std::cout << "pas trouvé" << std::endl;

    return 0;
}
```

## Conteneurs : map

- conteneur associatif clé-valeur
- recherche en temps logarithmique

* * *

```cpp
#include <iostream>
#include <map>

int main() {

    std::map<std::string,int> M {{"toto",13}, {"tata",37}};

    M["titi"] = 42;

    auto iter = M.find("tata");
    if (iter != M.end())
        std::cout << "trouvé : valeur=" 
                  << iter->second << std::endl;
    else
        std::cout << "pas trouvé" << std::endl;

    return 0;
}
```

## Itérateurs

- un itérateur est un pointeur associé à un conteneur
- permet de manipuler le conteneur :
    - parcourir le conteneur
    - ajouter/supprimer/modifier un élément dans le conteneur
    - ...
- utilisation :
    - récupérer un itérateur via les méthodes du conteneur (entre autres)
    - accéder à l’élément pointé via l’opérateur de déréférencement
    - passer à l’élément suivant via l’opérateur d’incrémentation

## Itérateurs : exemple d’utilisation

```cpp
std::vector<int> V {40, 21, 42};

// récupère un itérateur sur le premier élément et 
// ajoute 2 à cet élément
std::vector<int>::iterator iter = V.begin();
*iter += 2;

// passe à l'élément suivant pour le multiplier par 2
++iter;
*iter *= 2;

// affiche le conteneur résultat
for (int x : V)
    std::cout << x << std::endl;
```

## Itérateurs : exemple de parcours

```cpp
std::vector<int> V {40, 21, 42};

// parcours le conteneur avec un itérateur
for ( std::vector<int>::const_iterator it=V.begin(); 
      it!=V.end();
      ++it )
    std::cout << *it << std::endl;

// idem mais en utilisant le mot-clé auto
for (auto it=V.begin(); it!=V.end(); ++it)
    std::cout << *it << std::endl;

// parcours "range-based" 
// (raccourci d'écriture, utilise aussi un itérateur)
for (int x : V)
    std::cout << x << std::endl;
```

## Objets-fonctions

- la STL fournit quelques objets-fonctions : `multiplies`, `greater`...
- parfois pratiques combinés aux algorithmes de la STL

* * *

```cpp
// crée un objet-fonction pour tester si un entier 
// est supérieur à un autre
std::greater<int> g;

// appelle l'objet-fonction sur 2 entiers
std::cout << g(3, 2) << std::endl;

// utilise un objet-fonction pour trier un vector
// dans l'ordre décroissant
std::vector<int> V {42, 13, 37};
std::sort(V.begin(), V.end(), std::greater<int>());

// affiche le vector trié
for (int x : V)
    std::cout << x << ' ';
std::cout << std::endl;
```

## Algorithmes

- la STL fournit quelques algorithmes standards : `for_each`, `find`, `accumulate`, `sort`...
- ce qui permet d’appliquer un algorithme sur un conteneur
- en utilisant les itérateurs
- et des fonctions/objets-fonctions/lambda

## Algorithme `for_each`

- applique une ”fonction” sur tous les éléments d’un conteneur

* * *

```cpp
#include <algorithm>
#include <iostream>
#include <vector>

int main() {

    std::vector<int> V{39, 15, 267};

    // crée une lambda
    auto fAfficher = [](int n)
        { std::cout << n << std::endl; };

    // applique la lambda sur les éléments du conteneur
    std::for_each(V.begin(), V.end(), fAfficher);

    return 0;
}
```

## Algorithme accumulate

- applique une ”fonction” successivement sur un accumulateur et les éléments d’un conteneur

* * *

```cpp
#include <algorithm>
#include <iostream>
#include <vector>

int main() {

    std::vector<int> V{39, 15, 267};
    int s = std::accumulate(V.begin(), V.end(),
                            0, std::plus<int>());
    std::cout << "la somme est " << s << std::endl;

    return 0;
}
```

## Récapitulatif : parcourir un conteneur

```cpp
std::vector<int> V {42, 13, 37};

// parcours par accès aléatoire
for (unsigned i=0; i<V.size(); i++)
    std::cout << V[i] << std::endl;

// parcours par un itérateur
for (auto it=V.begin(); it!=V.end(); ++it)
    std::cout << *it << std::endl;

// parcours "range-based"
for (int x : V)
    std::cout << x << std::endl;

// parcours par l'algorithme for_each
auto f = [] (int x) { std::cout << x << std::endl; };
std::for_each(V.begin(), V.end(), f);
```

