---
title: CM C++, Fonction, etc...
date: 2019-04-19
---

## Fonction

- section de code avec un nom
- que l’on peut exécuter
- sur des arguments
- pour produire une valeur de retour

* * *

```cpp
// définit une fonction
int ajouterEntiers(int x, int y) {
    return x + y;
}

int main() {
    int z = ajouterEntiers(5, 37); // appelle la fonction
    std::cout << z << std::endl;
    return 0;
}
```

## Méthode

- fonction membre d’une classe/structure
- exécutable à partir d’une instance
- dans la portée de l’instance

* * *

```cpp
struct Accumulateur {
    int _resultat;
    Accumulateur() : _resultat(0) {}

    // définit une méthode (qui accède aux données membres)
    void ajouter(int x) { _resultat += x; } 
};

int main() {
    Accumulateur a;
    a.ajouter(5); // appelle la méthode à partir d'un objet
    a.ajouter(37);
    std::cout << a._resultat << std::endl;
    return 0;
}
```

## Opérateur

- fonction avec syntaxe particulière
- exemples : opérateurs arithmétiques, opérateurs booléens...
- peuvent être surdéfinis pour les nouvelles classes/structures

```cpp
// opérateur d'addition
int i = 5 + 37; 
// ou
int i = operator+(5, 37);

// opérateur d'égalité
bool b = i == 42; 
// ou
bool b = operator==(i, 42);
```

## Surdéfinition d’opérateurs (« méthode »)

- possible uniquement si le premier paramètre correspond à la classe

```cpp
class Personne {
    private:
        std::string _nom;
        int _age;
    // ...
    public:
        bool operator==(const Personne & p2) const {
            return _nom == p2._nom and _age == p2._age;
        }
};
```

* * *

```cpp
int main() {
    Personne toto("Toto", 42);
    Personne tata("Tata", 1337);

    if (toto == tata) // ou : if (toto.operator==(tata))
        // ...

    return 0;
}
```

## Surdéfinition d’opérateurs (« fonction »)

- problème de l’accès aux membres privés : accesseurs ou `friend`

```cpp
class Personne {
    // ...
    const std::string & getNom() const;
    int getAge() const;
    // ...
};

bool operator==(const Personne & p1, const Personne & p2) {
    return p1.getNom() == p2.getNom()
       and p1.getAge() == p2.getAge();
} 
```

* * *

```cpp
int main() {
    Personne toto("Toto", 42);
    Personne tata("Tata", 1337);

    if (toto == tata) // ou : if (operator==(toto, tata))
        // ...

    return 0;
}
```

## Objet-fonction

- instance d’une classe qui surdéfinit `operator()`
- généralise la notion de fonction dans le paradigme objet

* * *

```cpp
class Additionneur { // définit une classe d'objet-fonction
    public :
        int operator()(int x, int y) { return x + y; }
};

int main () {

    // instancie un objet-fonction de la classe Additionneur
    Additionneur obj_fct;

    // évalue l'objet-fonction (= appel de fonction)
    int n = obj_fct(4, 5); 

    return 0;
}
```

## Objet-fonction de la bibliothèque standard

```cpp
#include <functional>

int main () {

    // instancie un objet-fonction de la classe std::plus
    std::plus<int> additionneur;

    // evalue l'objet-fonction
    int s = additionneur(4, 5);

    return 0;
}
```

## Lambda

- fonction construite dynamiquement
- on peut contrôler sa fermeture (capture de variables, par valeur ou par référence), via les `[]`

* * *

```cpp
#include <functional>

int main () {

    // construit une lambda f1
    auto f1 = [] (int x, int y) -> int { return x+y; };

    // appelle la lambda f1
    f1(4, 5);

    // autre facon d'écrire la même lambda
    auto f2 = [] (int x, int y) { return x+y; };

    return 0;
}
```

## Lambda : captures de variables

```cpp
int n = 37;

auto f1 = [n](int x){ return n+x; };
// capture la valeur de n (37)

auto f2 = [&n](int x){ return n+x; };
// capteur une référence vers n

n = 2; // modifie la variable n

f1(5); // capture par valeur (37) -> retourne 42
f2(5); // capture par référence (variable n) -> retourne 7
```

## Le type std::function

- le C++ permet de manipuler des fonctions comme des variables classiques
- une fonction peut être évaluée, utilisée pour créer d’autres fonctions, passée en paramètre...
- le type `std::function` permet de représenter des fonctions

* * *

```cpp
int ajouterEntiers(int x, int y) { return x+y; }

struct Additionneur {
    int operator()(int x, int y) { return x+y; }
};
int main () {
    // fonction globale
    std::function<int(int,int)> f1 = ajouterEntiers;
    // objet-fonction
    Additionneur a2;
    std::function<int(int,int)> f2 = a2;
    // objet-fonction de std
    std::plus<int> a3;
    std::function<int(int,int)> f3 = a3;
    // lambda
    std::function<int(int,int)> f4 = [] (int x, int y) {
        return x+y; };
    int r1 = f1(5, 37);
    // ...
```

## Retourner une fonction avec une lambda

```cpp
std::function<void(int&)> creerIncrementeur(int n) {
    return [n] (int & x) { x += n; };
}

int main() {
    std::function<void(int&)> inc5 = creerIncrementeur(5);
    std::function<void(int&)> inc37 = creerIncrementeur(37);
    int r = 0;
    inc5(r);   // r vaut 5
    inc37(r);  // r vaut 42
    ...
```

