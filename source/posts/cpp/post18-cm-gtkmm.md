---
title: CM C++, Interfaces graphiques avec gtkmm
date: 2019-04-19
---

## Généralités sur gtkmm

- bibliothèque d'interface graphique
- version C++ de GTK+ (développé pour GIMP, utilisé dans Gnome)
- multi-plateforme, licence LGPL
- <http://www.gtkmm.org>
- doc [gtkmm-3.0](https://developer.gnome.org/gtkmm-tutorial/stable/)
- bibliothèques C++ concurrentes : Qt, wxWidgets, FLTK...

## Présentation technique

- surcouche complètement objet de GTK+ (pas de macro), widgets dérivables
- gestion de signaux, gestion mémoire, unicode
- ne réinvente pas la roue (utilise : STL, libsigc++, glib, gdk, pango, cairo, gtkglext...)
- utilitaire graphique de conception d’interfaces : Glade

## Éléments d’une interface graphique

- widgets : éléments (affichables ou non) d’une interface (fenêtre, bouton, zone de texte, menu...)
- conteneurs : widgets permettant de contenir/positionner d’autres widgets
- fenêtres : conteneurs autonomes, gérés par le système de fenêtres

## Mécanisme signal-slot

- une interface graphique réagit à des événements (clic souris, touche clavier, timer...)
- un événement produit un signal (objet Event)
- un signal est connecté à un slot (fonction) qui gère l’événement
- un widget peut définir des slots pour recevoir des signaux
- un widget peut émettre des signaux

## Organisation type d’une interface graphique

- une application
- une fenêtre principale
- des widgets interconnectés implémentant l’interface

## Fonctionnement type d’une interface graphique

#. les objets sont créés et initialisés (application, fenêtre principale, widgets, connexions)
#. l’application lance sa boucle événementielle qui :
    #. reçoit les signaux émis (stockés dans une file)
    #. les transmet aux slots auxquels ils sont connectés

    $\rightarrow$ s’il n’y a pas d’événement, l’application ne fait rien

## Dangers de la programmation événementielle

- la gestion d’un signal peut nécessiter d’émettre un autre signal
  $\rightarrow$ risque de boucle infinie

- la gestion d’un signal bloque la boucle événementielle $\rightarrow$
  risque de latence

- au lancement de la boucle événementielle, les widgets initialisés doivent
  être encore vivants $\rightarrow$ solution : déclarer les widgets en
  attributs ou utiliser Gtk::manage

## Quelques widgets

- `Label` : texte non éditable
- `Entry` : zone de saisie de texte
- `Button` : bouton
- `DrawingArea` : zone de dessin 2D

## Quelques conteneurs

- `Window` : fenêtre classique
- `MessageDialog` : fenêtre de message
- `Fixed` : conteneur pour positionner manuellement des widgets
- `HPaned` : conteneur définissant deux panneaux horizontaux
- `MenuBar ToolBar StatusBar`...

## Exemple 1

```cpp
// g++ `pkg-config --cflags --libs gtkmm-2.4` toto.cpp
#include <gtkmm.h>
int main(int argc, char ** argv) {
    Gtk::Main kit(argc, argv); // application gtkmm
    Gtk::Window window; // fenetre principale
    Gtk::Label label(" Hello world ! "); // message
    window.add(label);
    window.show_all();
    kit.run(window); // lance la boucle evenementielle
    return 0;
}
```

![](files/gtkmm_helloworld.png)

## Exemple 2

```cpp
class MaFenetre : public Gtk::Window {
    private:
        Gtk::HPaned _paned; // un panneau d'alignement
        Gtk::Label _label; // un texte
        Gtk::Button _button; // un bouton

    public:
        MaFenetre() : _paned(),
                      _label("un texte"), 
                      _button("un bouton")
        {
            _paned.add1(_label); // ajoute le texte 
            _paned.add2(_button); // ajoute le bouton
            add(_paned); // ajoute le panneau a la fenetre
            set_title("une fenetre");
            show_all(); // demande d'afficher les elements
        }
};
```

* * *

```cpp
int main(int argc, char ** argv) {
    Gtk::Main kit(argc, argv);
    MaFenetre fenetre;
    kit.run(fenetre);
    return 0;
}
```

![](files/gtkmm_fenetre.png)

## Gestion des signaux

- mécanisme signal-slot :
    - un événement utilisateur génère un signal
    - le signal est transmis à une fonction de rappel (slot)
    - la fonction de rappel gère le signal
- connecter un signal à une fonction de rappel :
    - soit surdéfinir une fonction de rappel de la classe mère
    - soit utiliser la fonctionnalité de connexion signal-slot

## Surdéfinir une fonction de rappel de la classe mère

```cpp
// definit une classe derivee pour notre bouton perso
class MonBouton : public Gtk::Button {
    // ...
    // surdefinit la fonction de rappel de la classe mère
    // dans la classe mère, cette fonction est virtuelle
    // => liaison dynamique
    void on_button_press_event(GdkEventButton*) override;
};
```

```cpp
void MonBouton::on_button_press_event(GdkEventButton*) {
    Gtk::Main::quit();
}
```

* * *

```cpp
// classe utilisant notre bouton perso
class MaFenetre : public Gtk::Window {
    // ...
    MonBouton _bouton;
};
```

## Utiliser la fonctionnalité de connexion signal-slot

```cpp
class MaFenetre : public Gtk::Window {
    // ...
    Gtk::Button _button; // widget pouvant générer un clic
    MaFenetre();
    void Quit(); // fonction à appeler pour gérer le clic
};
```

```cpp
MaFenetre::MaFenetre() {
    // ...
    // realise la connexion du signal avec la fonction
    _button.signal_button_press_event().connect(
        sigc::mem_fun(*this, &MaFenetre::Quit));
}
```

## Quelques signaux 

- clic souris, touche clavier, affichage, oisiveté...
- voir la doc du widget
- certaines fonctionnalités doivent être activées : `add_events(Gdk::BUTTON_PRESS_MASK)`

## Dessin 2D 

- widget `Gtk::DrawingArea`
- utilisation :
    - récupérer la fenêtre `Gdk::Window`
    - récupérer le contexte `Cairo::Context`
    - utiliser les fonctions de dessin de Cairo

* * *

```cpp
class Dessin : public Gtk::DrawingArea {
    // ...
    // surdefinit la fonction de rappel pour l'affichage
    bool on_draw(
        const Cairo::RefPtr<Cairo::Context> &) override;
};
```

```cpp
int main(int argc, char ** argv) {
    Gtk::Main kit(argc, argv);
    Gtk::Window window;
    Dessin dessin;
    window.add(dessin);
    window.show_all();
    kit.run(window);
    return 0;
}
```

* * *

```cpp
bool Dessin::on_draw(
    const Cairo::RefPtr<Cairo::Context> & context) {

    // règle le tracé
    context->set_source_rgb(1.0, 0.0, 0.0);
    context->set_line_width(10.0);

    // dessine une diagonale
    auto window = get_window();
    context->move_to(0, 0);
    context->line_to(window->get_width(), 
                     window->get_height());

    // met à jour l'affichage
    context->stroke();

    return true; // le signal a été géré
}
```

* * *

![](files/gtkmm_dessin.png)

