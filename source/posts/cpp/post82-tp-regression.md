---
title: TP C++, Régression polynomiale
date: 2019-10-14
---

## Explications

La régression polynomiale permet d'approximer des données par une fonction
polynomiale (droite, parabole...).  Soient, par exemple, les données suivantes,
issues de mesures physiques imparfaites :

* * *

![](files/regression_data.svg){width="70%"}

* * *

Ces données peuvent être représentées par une régression polynomiale : 

- d'ordre 1 (régression linéaire) : $f(x) = -0.247 - 0.289x$
- d'ordre 2 (régression quadratique) : $f(x) = -0.997 + 3.689x - 3.485x^2$
- ...

* * *

![](files/regression_fit.svg){width="70%"}

* * *

Pour résumer, on a $n$ données $(x,y)$ et on cherche le polynôme
$f$ de degré $d$ qui approxime le mieux ces données.  Écrit sous
forme matricielle, le problème revient à résoudre 
$\mathbf{A}\mathbf{f} = \mathbf{y}$ pour $\mathbf{f}$ :


$$\begin{bmatrix}
    1 & x_0 & \cdots & x_0^d \\
    1 & x_1 & \cdots & x_1^d \\
    \vdots & \vdots & \cdots & \vdots \\
    1 & x_n & \cdots & x_n^d \\
\end{bmatrix}
.
\begin{bmatrix}
    f_0\\
    f_1\\
    \vdots\\
    f_d\\
\end{bmatrix}
=
\begin{bmatrix}
    y_0\\
    y_1\\
    \vdots\\
    y_n\\
\end{bmatrix}$$


## Exercice

- Complétez le projet `TP_regression` fourni en implémentant une régression
  polynomiale.  Pour cela, utilisez la bibliothèque 
  [eigen3](http://eigen.tuxfamily.org/dox/index.html) 
  et notamment la 
  [méthode de la SVD](http://eigen.tuxfamily.org/dox/group__LeastSquares.html). 
  Par exemple, l'exécution de `./regression.out 2 50 > fit2.csv` doit écrire un fichier `fit2.csv` et afficher :

* * *


```text
dataX:
       0
0.114544
...
0.993443
       1

dataY:
        -1
 -0.617317
...
 -0.741438
 -0.713133

f:
-0.997471
   3.6899
 -3.48507
```

