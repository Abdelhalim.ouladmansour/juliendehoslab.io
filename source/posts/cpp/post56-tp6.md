---
title: TP6 C++, Opérateurs et entrées/sorties
date: 2019-04-19
---

On veut implémenter une gestion de bibliothèque basique. On propose la
modélisation suivante. Attention : dans ce cas simple, `Bibliotheque` dérive
d'une classe de la STL mais dans la vraie vie, il vaut mieux éviter ce genre de
dérivation.

* * *

![](uml/TP6a.svg){width="60%"}

## Mise en place du projet

- Utilisez le projet `TP6` fourni. 

## Module Livre

- Implémentez le module `Livre` du diagramme précédent. Dans le constructeur,
  vérifiez que les chaînes de caractères ne contiennent pas les caractères
  point-virgule ni retour chariot (si c'est le cas, levez une exception).
  Utilisez la méthode `find` de la classe `std::string` 
  (cf <http://en.cppreference.com>).

- Testez en décommentant les tests unitaires correspondants.

## Comparaison de livres

- Écrivez une **méthode** implémentant l'opérateur "inférieur strictement". On
  considère qu'un livre est inférieur à un autre si son auteur le précède dans
  l'ordre lexicographique; si les auteurs sont identiques on considère le
  titre; on ignore l'année. Utilisez les tests unitaires fournis.

- Écrivez une **fonction** implémentant l'opérateur d'égalité. On considère que
  deux livres sont égaux s'ils ont le même titre, le même auteur et la même
  année. Testez avec les tests unitaires.

## Opérateurs de flux

- Implémentez l'opérateur de flux de sortie (pour le format : titre, auteur et
  année, séparés par un point-virgule, cf les tests unitaires).  Testez avec
  les tests unitaires.

- Implémentez l'opérateur de flux d'entrée. Indication: utilisez la fonction
  `std::getline`.  Testez avec les tests unitaires.

## Module Bibliotheque

- Implémentez le module `Bibliotheque` (uniquement la méthode `afficher`, pour
  l'instant). Testez dans le `main`.

## Tri

- Implémentez la méthode `trierParAuteurEtTitre` en utilisant la fonction
  `sort` de la bibliothèque standard. Testez avec les tests unitaires.

- En utilisant une lambda, implémentez la méthode `trierParAnnee`. Testez avec
  les tests unitaires.

## Entrée/sortie fichier

- Implémentez les méthodes `ecrireFichier` et `lireFichier` (format : un
  livre sur une ligne).  Testez avec les tests unitaires.

