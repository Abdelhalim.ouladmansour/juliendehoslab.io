---
title: TP5 C++, Interface graphique
date: 2019-04-19
---

On veut réaliser une interface graphique permettant de visualiser les
figures géométriques que vous avez implémentées au TP précédent.

![](files/superviewer.png)

## Mise en place du projet

- Reprenez le projet du TP précédent.

## Première interface graphique

- Écrivez un programme principal qui affiche une fenêtre vide. Testez.

- Vous allez maintenant créer un module pour afficher votre fenêtre.  Pour
  cela, écrivez un module `ViewerFigures` correspondant au diagramme suivant.
  Modifiez votre programme principal. Testez.

![](uml/TP5a.svg){width="40%"}

- Ajoutez un titre à la fenêtre principale et réglez sa taille à $640\times 480$ (méthodes `set_title` et `set_default_size`).

## Zone de dessin

- Écrivez un module `ZoneDessin` permettant de gérer et d'afficher des
  figures géométriques au sein de votre `ViewerFigures`. Pour l'instant,
  n'implémentez pas complètement les méthodes (uniquement le nécessaire pour
  que çà compile). N'oubliez pas d'intégrer votre zone de dessin dans votre
  interface.

* * *

![](uml/TP5b.svg){width="50%"}

* * *

- Dans le constructeur, ajoutez quelques figures géométriques dans le tableau
  de figures. Libérez la mémoire dans le destructeur et vérifiez que votre
  gestion mémoire est correcte avec `valgrind`.

## Dessin 2D

- Implémentez la méthode `on_expose_event` de telle sorte qu'elle appelle
  l'affichage des figures (texte dans la console, pour l'instant).  Exécutez le
  programme et vérifiez le résultat. 

- Pourquoi la méthode `on_expose_event` est-elle exécutée alors que votre
  code ne l'appelle jamais.

- Modifiez les méthodes `afficher` des figures géométriques de telle
  sorte qu'elles dessinent graphiquement les figures.  Indication : passez un
  paramètre de type `const Cairo::RefPtr<Cairo::Context>`. 

## Ajouter et supprimer des figures

- Implémentez la méthode `ZoneDessin::gererClic` pour gérer le clic de
  souris.  Un clic gauche doit ajouter, dans le tableau de figures, un polygone
  régulier positionné à l'endroit du clic.  Un clic droit doit supprimer
  (proprement) la dernière figure ajoutée.  Indication : la méthode
  `Gdk::Window::invalidate` permet de rafraichir l'affichage.

- Connectez le signal `signal_button_press_event` à la méthode `gererClic`
  et activez la gestion de ce signal avec la méthode `add_events`. Testez.

- En résumé, quelles sont les deux façons de gérer un événement dans une
  interface graphique ?  Quel est l'avantage de chacune ?

## Figures aléatoires 

- Modifiez la méthode `ZoneDessin::gererClic` pour faire varier aléatoirement
  le rayon et le nombre de côtés des polygones réguliers.  Indication :
  utilisez la bibliothèque `random` (cf http://en.cppreference.com).

## S'il vous reste du temps

- Réimplémentez le tableau de pointeurs `ZoneDessin::_figures` par un tableau
  de `std::unique_ptr` et mettez à jour votre code en fonction.

- Vérifiez, avec `valgrind`, que votre gestion mémoire est correcte.

