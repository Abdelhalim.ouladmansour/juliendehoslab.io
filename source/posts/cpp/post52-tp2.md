---
title: TP2 C++, Pointeurs et mémoire
date: 2019-04-19
---

## Mise en place du projet

Comme pour le TP précédent, vous allez développer un projet avec un programme
principal et un programme de tests unitaires, en utilisant qtcreator et cmake/make. 

- Copiez le projet de base : `cp -r TP_vide TP2`. 

- Compilez le projet avec cmake et testez les exécutables produits.

- Quelles sont les étapes à réaliser pour ajouter un nouveau module au projet
  (fichiers, tests unitaires) ?

## Pointeurs et allocation dynamique

- Qu'est-ce-qu'un pointeur ? Quelles sont les quatre fonctionnalités de base
  des pointeurs ? Dans le programme principal, écrivez du code permettant de
  tester le scénario suivant :

    - créer un entier `a`
    - affecter la valeur 42 dans `a`
    - créer un pointeur d'entier `p` 
    - faire pointer `p` vers `a`
    - affecter la valeur 37 dans `a` via le pointeur

* * *

- Qu'est-ce-que l'allocation dynamique ? Quand l'utiliser ? Dans le
  programme principal, testez le scénario suivant :

    - créer un pointeur d'entier `t`
    - allouer un tableau de 10 entiers pointé par `t`
    - affecter la valeur 42 dans la troisième case du tableau
    - désallouer le tableau
    - réinitialiser le pointeur à `nullptr`

## Implémentation d'une liste chainée

On veut implémenter le diagramme de classes suivant.

![](uml/TP2.svg){width="30%"}

* * *

- Ajoutez un module `Liste` à votre projet.

- Dans ce module, implémentez les structures `Noeud` et `Liste`
  **progressivement** (en testant chaque méthode avec des tests unitaires,
  juste après avoir l'avoir écrite).  Pour l'instant, ignorez les libérations
  mémoires.

## Analyse des fuites mémoires

- Écrivez un programme principal qui crée une liste contenant les entiers 13 et
  37 puis l'affiche en utilisant une boucle `for`.  Vérifiez que votre
  programme compile et s'exécute.

- Exécutez votre programme avec `valgrind`. Que se passe-t-il ?  Pourquoi ?

- Ajoutez un destructeur dans votre module `Liste` pour résoudre le problème. 

## Débogage

- Lancez le débogueur. Placez un point d'arrêt dans la méthode `getElement`
  et dans le destructeur de `Liste` puis lancez le débogage.

- Testez les différentes fonctionnalités du débogueur : pile d'appels,
  exécution pas-à-pas, inspection de variable...

## S'il vous reste du temps

- [TP Régression polynomiale](post82-tp-regression.html)

- [TP C++, Complexité algorithmique](post80-tp-complexite.html)

