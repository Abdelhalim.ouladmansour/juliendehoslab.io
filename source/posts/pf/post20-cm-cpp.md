---
title: CM PF, C++
date: 2019-04-18
---

# Généralités

## Motivation

- beaucoup de langages impératifs/objets modernes intègrent des fonctionnalités de programmation fonctionnelle
- intérêt : expressivité, réduction des effets de bords (sécurité, parallélisme...)
- fonctionnalités : lambda, traitements de listes, algorithmes...
- langages : C++, C#, java, python, perl, javascript...

## Limitation

- intégration a posteriori plus ou moins heureuse
- quelques fonctionnalités uniquement
- ce ne sont que des outils, donc adaptés à certaines utilisations mais pas à toutes

## Programmation fonctionnelle et C++

- émergence à partir de la norme C++11
- quelques fonctionnalités uniquement mais intégrées assez proprement
- avant : utilisation d'objets-fonctions (joli en théorie mais imbitable en pratique)
- fonctionnalités :
    - lambda, évaluation partielle
    - objets-fonctions prédéfinis,
    - intégration aux conteneurs et itérateurs STL
    - algorithmes...

## Bibliographie

- [A Tour of C++](https://isocpp.org/tour)
- Bjarne Stroustrup
- Addison-Wesley Professional, 192 pages, 2013
- le C++ moderne en moins de 200 pages par l’auteur du C++

![](files/cm-cpp/livre_tour_cpp.png)

## Site de référence 

- <http://en.cppreference.com>

![](files/cm-cpp/cppreference.png)

# Fonctions

## Le type générique std::function

- wrapper de types appelables (sur lesquels on peut appeler `operator()`)
- permet de représenter des fonctions, méthodes, lambdas, évaluations partielles...
- le paramètre de template indique les types des paramètres et de retour

* * *

```cpp
#include <iostream>
#include <functional>
void printNum(int i)
{
    std::cout << i << std::endl;
}
int main() 
{
    // encapsule une fonction classique
    std::function<void(int)> f_display = printNum;
    f_display(-9);

    // encapsule une lambda
    std::function<void()> f_display_42 = []() 
        { printNum(42); };
    f_display_42();

    return 0;
}
```

## Lambda 

- construit une fermeture (fonction anonyme avec capture de variables)
- intérêt : créer une fonction selon un contexte particulier, passer une fonction en paramètre, retourner une fonction...
- capture par copie ou par référence

* * *

```cpp
auto doubler = [](int x) { return 2*x; };
std::cout << doubler(21) << std::endl;

auto afficher = [](int i){ std::cout << i << std::endl; };
afficher(42);

int nbAffichages = 0;
auto afficherCompter = [&nbAffichages](int i) 
{ 
    std::cout << i << std::endl; 
    nbAffichages++;
};
afficherCompter(42);
std::cout << "nbAffichages=" << nbAffichages << std::endl;
```

## Objets-fonctions prédéfinis de la STL

- permet de construire quelques fonctions classiques
- opérateurs arithmétiques, de comparaisons...

```cpp
// construit une fonction pour multiplier deux int
std::function<int(int,int)> multiplier 
    = std::multiplies<int>();

std::cout << multiplier(21, 2) << std::endl;
```

## Évaluation partielle 

- construit une fonction en évaluant partiellement une autre fonction
- syntaxe peu intuitive mais puissante

```cpp
using namespace std::placeholders;   // pour _1, _2...

std::function<int(int,int)> multiplier 
    = std::multiplies<int>();

// évaluation partielle de multiplier :
// -> 42 est le paramètre fixé pour évaluer multiplier
// -> _1 est le premier paramètre de multiplier42 et indique 
//    comment utiliser ce paramètre pour évaluer multiplier
std::function<int(int)> multiplier42 
    = std::bind(multiplier, 42, _1);

std::cout << multiplier42(2) << std::endl;
```

# Conteneurs

## Description 

- structures de données composées fournies par la STL
- pas grande chose à voir avec la programmation fonctionnelle si ce n'est que les fonctions, conteneurs, itérateurs et algorithmes sont compatibles
- accès aléatoire/séquentiel, complexités
- pour info : `pair`, `tuple`, mais pas de liste en compréhension

```cpp
// vector de 3 éléments (de valeur 1, 2 et 3, respectivement)
std::vector<int> u {1,2,3};

// vector de 200 éléments (de valeur 0)
std::vector<int> v(200,0);
```

## Quelques conteneurs 

- `array` : tableau de taille statique
- `vector` : tableau de taille dynamique
- `list` : liste chaînée
- `set` : ensemble d'éléments uniques (triés)
- `map` : dictionnaire (trié)
- `unordered_set` : `set` implémenté avec une fonction de hachage
- `unordered_map` : `map` implémenté avec une fonction de hachage
- ...

# Itérateurs 

## Description 

- permet d'uniformiser l'accès aux conteneurs

```cpp
// conteneur
std::vector<int> v {13,37,42};

// parcours du conteneur, version reloue
for ( std::vector<int>::iterator it=v.begin();
      it!=v.end();
      ++it)
    *it += 2;

// parcours du conteneur, version C++11
for (int & i : v)
    i += 2;
```

## Itérateurs d'insertion 

- les méthodes d'insertion diffèrent d'un conteneur à un autre
- les itérateurs et les méthodes `begin`/`end` uniformisent les accès sans modification de structure 
- les itérateurs d'insertion uniformisent les accès avec modification de structure (par exemple, ajouter des éléments à la fin d'un vector)

* * *

```cpp
std::vector<int> v {13,37,42};

auto iter0 = v.begin();
*iter0 = 51;
// ok : on modifie un élément existant mais pas la structure

auto iter1 = v.end();
*iter1 = 51;
// erreur : l'itérateur ne référence pas un élément valide
// et ne permet pas de modifier la structure

auto iter2 = std::back_inserter(v);
iter2 = 666;
*iter2 = 51; // syntaxe ok mais bof (operator* inutile)
```

# Algorithmes 

## Description

- algorithmes génériques applicables sur tout conteneur, via les itérateurs
- certains algorithmes particuliers sont implémentés comme méthodes de conteneurs (ex. `list::sort`)
- algorithmes classiques en programmation fonctionnelle :

    - mapping : `for_each` ou `transform`
    - reduction : `accumulate`
    - filtrage : `copy_if` ou `remove_if` + `erase`

## Exemple 

```cpp
std::vector<int> v;

// remplit v avec les entiers de 1 à 6 
int x = 1;
std::generate_n(std::back_inserter(v), 6, 
                [&x](){return x++;});

// calcule et affiche la somme des éléments de v
int r = std::accumulate(v.begin(), v.end(), 
                        0, std::plus<int>())
std::cout << r << std::endl;

// affiche les éléments de v
std::for_each(v.begin(), v.end(), 
              [](int n){ std::cout << n << ' ';});
std::cout << std::endl;
```

* * *

```cpp
// construit une lambda pour écrire v dans un flux
auto printV = [&v](std::ostream & os) {
    std::for_each(v.begin(), v.end(),
                  [&os](int n){ os << n << ' ';});
    os << std::endl;
};

// écrit v à l'écran en utilisant la lambda
printV(std::cout);

// écrit v dans un fichier en utilisant la lambda
std::ofstream file("out.txt");
printV(file);
```

