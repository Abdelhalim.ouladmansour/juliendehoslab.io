---
title: Bibliographie PF (dispo à la BU)
date: 2019-04-18
---

## Programming in Haskell

- Graham Hutton
- 181 pages, Cambridge University Press, 2007
- support de cours concis sur Haskell
- [ressources en ligne](http://www.cs.nott.ac.uk/~gmh)

![](files/biblio/livre_hutton.jpg)

## Learn You a Haskell for Great Good!

- Miran Lipovača
- 400 pages, No Starch Press, 2011
- intro Haskell populaire, avec de l'humour
- [version en ligne](http://learnyouahaskell.com)
- [traduction française (non-officielle)](http://lyah.haskell.fr)

![](files/biblio/livre_lipovaca.png)

## Thinking Functionally with Haskell

- Richard Bird
- 354 pages, Cambridge University Press, 2014
- intro Haskell « style classique »

![](files/biblio/livre_bird.jpg)

## Haskell: The Craft of Functional Programming

- Simon Thompson
- 608 pages, Addison Wesley, 2011
- intro Haskell + utilisation au quotidien

![](files/biblio/livre_thompson.jpg)

## Real World Haskell

- Bryan O'sullivan, John Goerzen, Don Stewart 
- 710 pages, O'Reilly, 2008
- intro Haskell classique, exemples réalistes 
- [version en ligne](http://book.realworldhaskell.org)

![](files/biblio/livre_osullivan.jpg)

## Mini-manuel de Programmation fonctionnelle

- Éric Violard
- 224 pages, Dunod, 2014 
- intro programmation fonctionnelle en général

![](files/biblio/livre_violard.jpg)


