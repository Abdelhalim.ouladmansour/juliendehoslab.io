---
title: TP PF, C++
date: 2019-04-18
---

# Exercices

> Utilisez le dossier `TP_cpp/exos` du dépôt de code <br>
> (un `.cpp` = un programme).

## Fonctions

Écrivez un code C++ équivalent au code haskell suivant.

```haskell
fonction :: Int -> Int -> Int
fonction x y = x + y

main = do
    print $ fonction 13 37
    print $ lambda 13 37
    print $ objetfonction 13 37
    print $ fonctionpartielle 2
        where lambda = (\x y -> x + y)
              objetfonction = (+)
              fonctionpartielle = fonction 42
```

## Mapping

- Écrivez un code C++ équivalent au code haskell suivant (utilisez `transform`
  pour le `map` et `for_each` pour les `mapM_`).
- Écrivez une version impérative (avec des boucles `for`).

```haskell
main = do
    mapM_ print data1
    mapM_ print data2
        where data1 = [1,2,3]
              data2 = map (\x -> x + 42) data1
```

## Réduction

- Écrivez un code C++ équivalent au code haskell suivant.
- Écrivez une version impérative.

```haskell
main = do
    print $ foldl (\x acc -> x+acc) 0 data
    print $ foldl (+) 0 data
    print $ foldl (\x acc -> if x>acc then x else acc) 0 data
    print $ maximum data
        where data = [1,2,3]
```

## Filtrage

- Écrivez un code C++ équivalent au code haskell suivant (avec et sans copie).
- Écrivez une version impérative (avec copie uniquement).

```haskell
main = do
    print $ filter (\ x -> x > 0) data
    print $ filter ((<) 0) data
        where data = [1,-2,3]
```

## Autres algorithmes 

Écrivez un code C++ équivalent au code haskell suivant (en utilisant les éléments de la STL indiqués).

```haskell
import Data.List (sort)
main = do
    print $ sum $ map (\x -> x*x) data  -- std::inner_product
    print $ any (\x -> x>0) data        -- std::any_of
    print $ sort data                   -- std::sort
        where data = [1,-2,3]           -- std::vector
```

# Mini projets

## Ré-écriture de code C++

Le dépôt de code contient trois projet C++ :

- `cubesampler` : un programme d'échantillonnage de sphère nD 
- `equalizer` : un programme d'égalisation d'histogramme d'images PGM 
- `42vainc` : un jeu abstrait 

Ré-écrivez les boucles `for` de ces programmes en utilisant les algorithmes
de la STL (cf les `TODO`).  Attention, dans la vraie vie réelle, une boucle
`for` est parfois plus lisible...

