---
title: Accès par proxy
date: 2019-04-14
notoc: notoc
---

Sur les machines de la fac, vous devez configurer les paramètres du proxy
pour que git (entre autres) puisse accéder à l'extérieur. Vous pouvez, par
exemple, ajouter les lignes suivantes dans votre fichier `~/.bashrc` :

```bash
export http_proxy="http://login:passwd@192.168.22.62:3128"
export https_proxy=$http_proxy
```

* * *

>
>    **METTEZ LE LOGIN:PASSWD DÉDIÉ AU PROXY ET NON VOS IDENTIFIANTS PERSONNELS !!!**
>
>    (si besoin, demandez ces identifiants à votre prof ou à l'admin réseau)

* * *

Pour prendre en compte les modifications du `.bashrc`, vous devez réouvrir
votre shell ou exécuter `source ~/.bashrc`.

