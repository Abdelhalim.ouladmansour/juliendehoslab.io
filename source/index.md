---
title: Julien Dehos' webpage
---

I am an assistant professor in computer science and this is my webpage.

## Affiliations & contact

- [Université du Littoral Côte d'Opale](http://www.univ-littoral.fr)
- E-mail: dehos at univ-littoral dot fr

## Teaching

- [Langage C++ (L3 Info)](posts/cpp/index.html)
- [Génie logiciel (L3 Info)](posts/gl/index.html)
- [Projet C++/GL (L3 Info)](posts/projet-cpp-gl/index.html)
- [Programmation fonctionnelle (Master Info)](posts/pf/index.html)
- [Programmation fonctionnelle pour le web (Master Info)](posts/pfw/index.html)
- [Calcul haute performance (Master Info)](posts/hpc/index.html)
- [Éditeurs de texte](posts/editeurs/index.html)
- [Environnement de travail](posts/env/index.html)

## Research

Publications:

- [Selected publications on HAL](https://hal.archives-ouvertes.fr/search/index/q/*/authFullName_s/Julien+Dehos/sort/producedDate_tdate+desc/)
- [Full list in my CV](cv/cv-dehos.pdf)

Selected work:

- [Multi-armed bandit for stratified sampling: Application to numerical integration](https://github.com/florianLepretre/ucbature)
- [Calculer des images de synthèse](https://gitlab.com/juliendehos/conf_sts_2016_se)
- [The Hex and Havannah connection games with MCTS-based AI players](https://github.com/fteytaud/hex_hav)
- [Image multi-scène par intégration d’un autostéréogramme dans une scène 3D](https://gitlab.com/juliendehos/spation)

## Software development

Repositories:

- <https://gitlab.com/users/juliendehos/projects>

Communications:

- An overview of the Nix ecosystem, [LilleFP14](https://www.meetup.com/fr-FR/Lille-FP/events/260541114/), 2019 :
  [slides](https://juliendehos.gitlab.io/lillefp-2019-nix),
  [dépot de code](https://gitlab.com/juliendehos/lillefp-2019-nix).
- Isomorphic web apps in Haskell, [LilleFP12](https://www.meetup.com/fr-FR/Lille-FP/events/258682124/), 2019 :
  [slides](https://juliendehos.gitlab.io/lillefp-2019-isomorphic),
  [dépot de code](https://gitlab.com/juliendehos/lillefp-2019-isomorphic).
- L'écosystème Nix pour développer en Python, et au delà…
  [PyconFR 2018](https://www.pycon.fr/2018/) :
  [vidéo](https://www.youtube.com/watch?v=rmRy9QZLp-g),
  [slides](https://juliendehos.gitlab.io/pyconfr-2018-nix/),
  [dépôt de code](https://gitlab.com/juliendehos/pyconfr-2018-nix).

Publications:

- [Apprendre comment implémenter une IA de jeu en Haskell](https://juliendehos.developpez.com/tutoriels/haskell/implementer-ia-jeu-en-Haskell),
  Developpez.com, 2019.
- [Apprendre comment implémenter un serveur de blog avec le langage Haskell](https://juliendehos.developpez.com/tutoriels/haskell/implementer-serveur-blog),
  Developpez.com, 2019.
- La programmation fonctionnelle - Introduction et application en
  Haskell à l'usage de l'étudiant et du développeur, Ellipses, 2019 :
  [page de l'éditeur](https://www.editions-ellipses.fr/programmation-fonctionnelle-introduction-applications-haskell-lusage-ltudiant-dveloppeur-p-13083.html),
  [dépôt de code](https://gitlab.com/juliendehos/codes-livre-haskell).

[![livre haskell](files/livre-haskell-jd-small.jpg)](files/livre-haskell-jd.jpg)


[//]: # (This may be the most platform independent comment)

