{-# LANGUAGE OverloadedStrings #-}

import Clay
import Data.Text.Lazy.IO (putStr)
import Prelude hiding (putStr, div)

main :: IO ()
main = putStr $ renderWith compact [] $ do

    hr ? border none nil white

    a ? do
        textDecoration none
        color "#00f"

    (h1 <> h2 <> h3) ? a ? do
        color black
        textDecoration none

    (td <> h1 <> h2 <> h3) |> a |> img ?
        display inline

    h1 ? fontSize (em 2)
    h2 ? fontSize (em 1.5)
    h3 ? fontSize (em 1.2)

    video <> img ? do
        display block
        marginLeft auto
        marginRight auto

    figcaption ?
        textAlign (alignSide sideCenter)

    video ?
        border solid (px 1) black

    html ? do
        margin auto auto auto auto
        maxWidth (px 900)
        width (pct 100)
        fontFamily ["Helvetica", "sans-serif"] [sansSerif]
        fontSize (pt 14)
        backgroundColor "#ddd"
        -- backgroundColor "#bbb"

    body ? do
        padding (px 10) (px 10) (px 10) (px 10)
        -- backgroundColor "#ddd"

    "#mysite" ?
        fontSize (em 1.5)

    "#mytitle" ? 
        textAlign (alignSide sideCenter)

    "#mydate" ? 
        textAlign (alignSide sideRight)

    "#mytopic" <> "#mysite" ? do
        color black
        fontWeight bold
        textDecoration none

    table <> th <> td ? do
        border solid (px 2) black
        borderCollapse collapse
        padding (px 5) (px 5) (px 5) (px 5)

    table ? do
        marginLeft auto
        marginRight auto

    main_ ? do
        borderTop solid (px 2) black
        borderBottom solid (px 2) black

    main_ <> header <> footer ? do
        padding nil (px 20) nil (px 20)
        margin (px 20) nil (px 20) nil

    footer ? 
        textAlign (alignSide sideRight)

    (p <> li <> td) |> code ? color "#060"

    code ? fontSize (pt 12)

    pre # ".text" <> div # ".sourceCode" ? do
        border solid (px 2) black
        padding (px 10) (px 10) (px 10) (px 10)
        backgroundColor "#ffc"
        overflow auto
        color black

    ul ? listStyleType none

    li # ".post" ? do
        display flex
        alignItems center

    ul |> li # before ? do
        content (stringContent "-")
        position absolute
        marginLeft (em (-1))

    "#sidebar-container" ? 
        width (pct 100)

    "#sidebar-content" ? do
        verticalAlign vAlignTop
        width (pct 100)

    "#sidebar-toc" ? do
        verticalAlign vAlignTop
        float floatRight
        maxWidth (px 250)
        backgroundColor "#ccc"
        margin (px 10) (px 10) (px 10) (px 10)
        padding (px 0) (px 10) (px 0) (px 10)
        border solid (px 2) black

    "#sidebar-toc" ? ul ? do
        margin (px 10) nil (px 10) nil
        paddingLeft (px 20)

    "#title" ? do
        fontVariant smallCaps
        textAlign center

    img # ".icon" ? do
        border solid (px 0) black
        margin (px 5) (px 5) nil (px 5) 
        width (px 32)

    img # ".icon-inline" ? do
        border solid (px 0) black
        margin (px 5) nil nil (px 10)
        width (px 24)

    table # ".myfloat" ? do 
        border solid (px 0) black
        width (pct 100)

    td # ".myfloat-left" <> td # ".myfloat-center" <> td # ".myfloat-right" ? do
        border solid (px 0) black
        padding (px 5) (px 5) nil (px 5)
        margin (px 5) (px 5) nil (px 5)
        width (pct 33)

    ".myfloat-left" ? textAlign (alignSide sideLeft)

    ".myfloat-center" ? textAlign (alignSide sideCenter)

    ".myfloat-right" ? textAlign (alignSide sideRight)

    table ?
        marginBottom (px 20)

    blockquote |> p ? margin (px 0) (px 0) (px 0) (px 0)

    blockquote ? do
        display inlineBlock
        padding (px 10) (px 10) (px 10) (px 10)
        border solid (px 2) black
        margin (px 20) auto (px 20) auto
        backgroundColor "#fbb"

