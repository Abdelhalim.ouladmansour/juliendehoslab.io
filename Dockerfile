FROM nixos/nix
RUN nix-channel --add https://github.com/NixOS/nixpkgs/archive/4d403a3a51663f30efd478c8f35f3c5b49c6bc37.tar.gz nixpkgs
RUN nix-channel --update
RUN nix-env -iA nixpkgs.texlive.combined.scheme-full

# docker build -t juliendehos/cicd:mywebpage .
# docker push juliendehos/cicd:mywebpage

