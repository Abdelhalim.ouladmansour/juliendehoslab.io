build:
	cabal build 

run:
	LATEXFILTER=$(shell find dist-newstyle -type f -name latexfilter) cabal run site build

gzip:
	find public \( -name '*.html' -o -name '*.css' -o -name '*.js' -o -name '*.svg' \) -print0 | xargs -0 gzip -9 -k -f

serve:
	python3 -m http.server -d public 3000

deploy:
	LATEXFILTER=$(shell find dist-newstyle -type f -name latexfilter) cabal run site deploy

clean:
	rm -rf _cache dist-newstyle public

