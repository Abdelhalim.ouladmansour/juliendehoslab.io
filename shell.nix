let

  rev = "4d403a3a51663f30efd478c8f35f3c5b49c6bc37";  # 2019-12-27
  url =  "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  channel = fetchTarball url;
  pkgs = import channel {};

  mywebpage = pkgs.haskellPackages.callCabal2nix "mywebpage" ./. {};

  deps = [
    pkgs.gnumake
    pkgs.graphviz
    pkgs.gzip
    pkgs.haskellPackages.cabal-install
    pkgs.librsvg
    pkgs.pandoc
    pkgs.pdf2svg
    pkgs.plantuml
    pkgs.python3
    pkgs.texlive.combined.scheme-full
  ];

in (pkgs.haskell.lib.addBuildTools mywebpage deps).env

