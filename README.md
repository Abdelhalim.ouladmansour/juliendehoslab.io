# website-hakyll

This is the source code of
[my website](https://juliendehos.gitlab.io).
This website is statically generated by [Hakyll](https://jaspervdj.be/hakyll):

- Generates HTML pages, slideshows and PDFs (with navigation links and tables of contents) from markdown files.
- Generates SVG graphics from plantuml files, graphivz/dot files and tikz files.
- The site generator, pandoc filter, HTML templates and CSS stylesheets are implemented in Haskell.

## Documentation

### Build

- install Nix, then:

```
nix-shell 
make build
make run
make gzip
make serve
make clean
exit
```

### Add a new topic 

- add a new folder in `source/posts`, e.g. `source/posts/mytopic`
- add an index file in this folder, e.g. `source/posts/mytopic/index.md`
- add content files in this folder (with names beginning by `post`), e.g. `source/posts/mytopic/post-mypost1.md`
- add files (images...) in a folder `files`, e.g. `source/posts/mytopic/files/myimg1.png`
- add uml files in a folder `uml`, e.g. `source/posts/mytopic/uml/myuml1.uml`
- add graphviz/dot files in a folder `dot`, e.g. `source/posts/mytopic/dot/mydot1.dot`
- add tikz files in a folder `tikz`, e.g. `source/posts/mytopic/tikz/mytikz1.tex`

### Mardown files

- to remove the table of contents: add `notoc: notoc` in the header

### Image

- use the `script/convert-*.sh` scripts to set image density (e.g. 150 dpi)

- for SVG files, set the image width (in percent) in the markdown file:

```
![](tikz/mytikz1.svg){width="90%"}`
```

- to put an image in a figure, set a caption:

```
![image in a figure, with a caption](files/myimg1.png)
```

### Video

- in the markdown file, include the video with an alternate image:

```
<video preload="metadata" controls>
<source src="files/wyvideo1.mp4" type="video/mp4">
![](files/wyvideo1.png)

</video>
```
- use the `script/extract1.sh` script to extract an image from a video

### Force next slide

- to force next slide in a slideshow, add the following line in the markdown file:

```
* * *
```

- to force next slide in a PDF, add the following line in the markdown file:

```
\newpage
```

